//
//  DMarketViewController.swift
//  PerfectPick
//
//  Created by Jhon Villalobos on 7/9/16.
//  Copyright © 2016 Jhon Villalobos. All rights reserved.
//

import UIKit
import SwiftSpinner
import Locksmith

class DMarketViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UITabBarDelegate {
    
    @IBOutlet weak var titleContainer: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tabBar: UITabBar!
    
    var orderList = [Order]()
    var preparedList = [Order]()
    
    var refreshControl : UIRefreshControl!

    override func viewDidLoad() {
        super.viewDidLoad()
        checkIfDriver()

        self.title = "Market Order List"

        self.tabBar.delegate = self
        self.tabBar.barTintColor = UIColor.black
        
        self.titleContainer.tintColor = UIColor.white
        self.titleContainer.backgroundColor = UIColor(colorLiteralRed: 60/255.0, green: 60/255.0, blue: 60/255.0, alpha: 1.0)
        self.titleContainer.layer.cornerRadius = 5.0
        
        self.getOrder()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: TableView Delegates
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DMarketCell") as! DMarketTableViewCell
        
        let itemRow = self.orderList[indexPath.row]
        cell.itemName.text  = itemRow.itemName
        cell.itemBoxes.text = "\(itemRow.qty)"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        var numSections = 0
        if (self.orderList.count > 0) {
            numSections = 1
        } else {
            let noDataLabel: UILabel = UILabel.init(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
            noDataLabel.text = "There no order available yet. \n Reasons Why? \n 1. Market never made an order. \n 2. Warehouse has yet to send order request to you."
            noDataLabel.numberOfLines = 6
            noDataLabel.textColor = UIColor.red
            noDataLabel.textAlignment = .center
            tableView.backgroundView = noDataLabel
            tableView.separatorStyle = .none
        }
        return numSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.orderList.count
    }
    
    
    //MARK: Network request
    func getOrder() {
        let orderStatus = Status.StatusLibrary().library["Loading"]
        let parameters = ["key": API_KEY, "status": orderStatus!] as AnyObject
        let methodCall = "\(URLServer)getMarketOrder"
        makeRequest(methodCall, metodo: .get, params: parameters) {
            response, error in
            if let ans = response {
                if ans["success"] as! Bool == true {
                    let content = ans["conent"] as! [[String:AnyObject]]
                    let itemsOrdered = content.count
                    self.orderList.removeAll()
                    if itemsOrdered > 0 {
                        for prd in content {
                            let idRow    = Int(prd["id"] as! String)!
                            let qtyItem  = Int(prd["quantity"] as! String)!
                            let qtyConfirmed = Int(prd["quantity"] as! String)!
                            let dateItem = convertStringDate(prd["date"] as! String)
                            let locat    = Int(prd["locations_id"] as! String)!
                            let itId     = Int(prd["items_id"] as! String)!
                            let userId   = Int(prd["users_id"] as! String)!
                            let itemName = prd["name"] as! String
                            let orderId  = Int(prd["orders_id"] as! String)!
                            let itemStatus = Int(prd["confirmed"] as! String)
                            let locationName = prd["location"] as! String
                            let switchStatus = (itemStatus == 1 ? true : false)
                            
                            let fillingList : Order = Order(id: idRow, qty: qtyItem, qtyConfirmed: qtyConfirmed, date: dateItem, locId: locat, itemId: itId, marketId: userId, itemName: itemName, status: switchStatus, orderId: orderId, locationName: locationName)
                            self.orderList.append(fillingList)
                        }
                    } else {
                        self.showMessage("No pending orders")
                    }
                    self.do_cell_refresh()
                } else {
                    self.showMessage(ans["message"] as! String)
                }
            } else {
                self.showMessage(NETWORK_ERROR)
            }
        }
    }
    
    
    func do_cell_refresh() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView?.reloadData()
        return
    }
    
    func updateTable() {
        self.orderList.removeAll()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.getOrder()
    }
    
    func showMessage(_ message: String) {
        let alert = UIAlertController(title: APP_NAME, message: message, preferredStyle: .alert)
        let cancel = UIAlertAction(title: "Close", style: .cancel, handler: { (action) -> Void in })
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
    }

    
    //MARK: Bottom Tabs actions
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        switch item.tag {
        case 1:
            let market = self.storyboard?.instantiateViewController(withIdentifier: "DMain") as! DMainViewController
            self.navigationController?.pushViewController(market, animated: true)
        case 2:
            let inventory = self.storyboard?.instantiateViewController(withIdentifier: "DInventory") as! DInventoryViewController
            self.navigationController?.pushViewController(inventory, animated: true)
        default:
            return
        }
    }
    
    func checkIfDriver() {
        var role = 0
        if let dictionary = Locksmith.loadDataForUserAccount(userAccount: "user") {
            role = dictionary["role"] as! Int
        }
        
        if role != 2 {
            deleteLogOut()
            let out = self.storyboard?.instantiateViewController(withIdentifier: "Home") as! HomeViewController
            self.navigationController?.pushViewController(out, animated: true)
        }
    }
    

}

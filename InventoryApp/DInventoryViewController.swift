//
//  DInventoryViewController.swift
//  PerfectPick
//
//  Created by Jhon Villalobos on 7/9/16.
//  Copyright © 2016 Jhon Villalobos. All rights reserved.
//

import UIKit
import SwiftSpinner
import Locksmith

extension UIView {
    func rotate(_ toValue: CGFloat, duration: CFTimeInterval = 0.2, completionDelegate: AnyObject? = nil) {
        let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation")
        rotateAnimation.toValue = toValue
        rotateAnimation.duration = duration
        rotateAnimation.isRemovedOnCompletion = false
        rotateAnimation.fillMode = kCAFillModeForwards
        
        self.layer.add(rotateAnimation, forKey: nil)
    }
}

class DInventoryViewController: UIViewController, UITabBarDelegate, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var titleContainer: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tabBar: UITabBar!
    @IBOutlet weak var btnTitleSkids: UIButton!
    
    var listItems = [CollapsedProducts]()
    var productList = [Product]()
    
    var isSkids = true
    
    var refreshControl : UIRefreshControl!
    
    let searchController = UISearchController(searchResultsController: nil)
    var filteredItems    = [Product]()
    var itemsForFilter   = [Product]()
 
    override func viewDidLoad() {
        super.viewDidLoad()
        checkIfDriver()

        self.title = "Warehouse Inventory"
        self.titleContainer.backgroundColor = UIColor(colorLiteralRed: 60/255.0, green: 60/255.0, blue: 60/255.0, alpha: 1.0)
        self.titleContainer.layer.cornerRadius = 5.0
        
        self.tabBar.barTintColor = UIColor.black
        self.tabBar.delegate = self
        
        //Search functions
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        definesPresentationContext = true
        tableView.tableHeaderView = searchController.searchBar
        
        loadRefreshControl()
        getInventory()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func getInventory() {
        SwiftSpinner.show("loading...")
        let parameters = [] as AnyObject
        let methodCall = "\(URLServer)getInventoryAlphabetically"
        makeRequest(methodCall, metodo: .get, params: parameters) {
            response, error in
            if let ans = response {
                let content = ans["content"] as! [[String:AnyObject]]
                self.listItems.removeAll()
                self.itemsForFilter.removeAll()
                for prd in content {
                    self.productList.removeAll()
                    let newItem : CollapsedProducts = CollapsedProducts(categoryName: "", items: self.productList)
                    let categoryName = prd["name"] as! String
                    if let tal = prd["items"] {
                        let items = tal as! [[String:AnyObject]]
                        for e in items {
                            let itemId    = Int(e["items_id"] as! String)
                            let itemName  = e["description"] as! String
                            let itemPrice = Double(e["price"] as! String)
                            var itemQty = Double(0)
                            if let validQty = Double(e["quantity"] as! String) {
                                itemQty = validQty
                            }
                            let itemSkids = Int(e["skids"] as! String)
                            let totalBoxes = Int(e["total_boxes"] as! String)
                            let internList : Product = Product(id: itemId!, name: itemName, price: itemPrice!, quantity: itemQty, skids: itemSkids!, totalBoxes: totalBoxes!)
                            self.productList.append(internList)
                            self.itemsForFilter.append(internList)
                        }
                    } else {
                        let internList : Product = Product(id: 0, name: "", price: 0, quantity: 0, skids: 0, totalBoxes: 0)
                        self.productList.append(internList)
                    }
                    newItem.categoryName = categoryName
                    newItem.items =  self.productList
                    self.listItems.append(newItem)
                }
                self.do_cell_refresh()
            } else {
                SwiftSpinner.hide()
                globalMessage(APP_NAME as NSString, msgBody: NETWORK_ERROR as NSString, delegate: nil, self: self)
            }
        }
    }
    
    // MARK : Table Delegates
    func numberOfSections(in tableView: UITableView) -> Int {
        if searchController.isActive && searchController.searchBar.text != "" {
            return 1
        }
        return listItems.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchController.isActive && searchController.searchBar.text != "" {
            return filteredItems.count
        }
        return (listItems[section].collapsed!) ? 0 : listItems[section].items.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableCell(withIdentifier: "DHeaderInventory") as! DInventoryHeaderCell
        
        if searchController.isActive && searchController.searchBar.text != "" {
            header.titleButton.setTitle("Filtered List", for: UIControlState())
            return header.contentView
        }
        
        header.toggleButton.tag = section
        header.titleButton.tag = section
        header.titleButton.setTitle(listItems[section].categoryName, for: UIControlState())
        header.toggleButton.rotate(listItems[section].collapsed! ? 0.0 : CGFloat(Double.pi / 2))
        header.titleButton.addTarget(self, action: #selector(self.toggleCollapse(_:)), for: .touchUpInside)
        header.toggleButton.addTarget(self, action: #selector(self.toggleCollapse), for: .touchUpInside)
        
        return header.contentView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DInventoryCell", for: indexPath) as! DInventoryTableViewCell
        
        if listItems.count > 0 {
            let dataCell: Product
            if searchController.isActive && searchController.searchBar.text != "" {
                dataCell = filteredItems[indexPath.row]
            } else {
                dataCell = listItems[indexPath.section].items[indexPath.row]
            }
            
            cell.itemName.text      = dataCell.name
            switch isSkids {
            case true:
                self.btnTitleSkids.setTitle("# of Skids", for: UIControlState())
                cell.itemBoxes.text = "\(dataCell.quantity!)"
            case false:
                self.btnTitleSkids.setTitle("# of Boxes", for: UIControlState())
                cell.itemBoxes.text = "\(dataCell.totalBoxes!)"
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50.0
    }
    
    //this is the function wheren the header collapse the rows
    func toggleCollapse(_ sender: UIButton) {
        /*
        let section = sender.tag
        let collapsed = listItems[section].collapsed // sections[section].collapsed
        
        // Toggle collapse
        listItems[section].collapsed = !collapsed
        
        // Reload section
        tableView.reloadSections(NSIndexSet(index: section), withRowAnimation: .Automatic)
 */
    }
    
    
    func do_cell_refresh() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView?.reloadData()
        SwiftSpinner.hide()
    }

    // ******
    
    @IBAction func buttonRequestMarket(_ sender: AnyObject) {
        
    }
    
    func showMessage(_ message: String) {
        let alert = UIAlertController(title: APP_NAME, message: message, preferredStyle: .alert)
        let cancel = UIAlertAction(title: "Close", style: .cancel, handler: { (action) -> Void in })
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
    }
    
    func updateTable() {
        self.listItems.removeAll()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.getInventory()
    }
    
    func loadRefreshControl() {
        refreshControl = UIRefreshControl()
        refreshControl.backgroundColor = UIColor.red
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to update data")
        refreshControl.addTarget(self, action: #selector(WMainViewController.refreshTable), for: .valueChanged)
        self.tableView.addSubview(refreshControl)
    }
    
    func refreshTable() {
        updateTable()
        refreshControl.endRefreshing()
    }
    
    //MARK: Bottom Tabs actions
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        switch item.tag {
        case 1:
            let driver = self.storyboard?.instantiateViewController(withIdentifier: "DMain") as! DMainViewController
            self.navigationController?.pushViewController(driver, animated: true)
        case 3:
            let market = self.storyboard?.instantiateViewController(withIdentifier: "DMarket") as! DMarketViewController
            self.navigationController?.pushViewController(market, animated: true)
        default:
            return
        }
    }
    
    func checkIfDriver() {
        var role = 0
        if let dictionary = Locksmith.loadDataForUserAccount(userAccount: "user") {
            role = dictionary["role"] as! Int
        }
        
        if role != 2 {
            deleteLogOut()
            let out = self.storyboard?.instantiateViewController(withIdentifier: "Home") as! HomeViewController
            self.navigationController?.pushViewController(out, animated: true)
        }
    }
    
    @IBAction func changeSkidsView(_ sender: AnyObject) {
        /*
        switch isSkids {
        case true:
            isSkids = false
        default:
            isSkids = true
        }
        self.do_cell_refresh()*/
    }
    
    //MARK: SearchBar
    func filterContentForSearchText(_ searchText: String, scope: String = "All") {
        filteredItems = itemsForFilter.filter { product in
            return product.name.lowercased().contains(searchText.lowercased())
        }
        tableView.reloadData()
    }
}

extension DInventoryViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        filterContentForSearchText(searchController.searchBar.text!)
    }
}

//
//  WMainTableViewCell.swift
//  InventoryApp
//
//  Created by Jhon Villalobos on 7/1/16.
//  Copyright © 2016 Jhon Villalobos. All rights reserved.
//

import UIKit

class WMainTableViewCell: UITableViewCell {
    
    @IBOutlet weak var viewCellContainer: UIView!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var productQuantity: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

//
//  MMakeOrderTableViewCell.swift
//  PerfectPick
//
//  Created by Jhon Villalobos on 7/8/16.
//  Copyright © 2016 Jhon Villalobos. All rights reserved.
//

import UIKit

class MMakeOrderTableViewCell: UITableViewCell {
    
    @IBOutlet weak var itemSelected: UISwitch!
    @IBOutlet weak var itemName: UILabel!
    @IBOutlet weak var qtySelected: UITextField!
    @IBOutlet weak var itemQty: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}

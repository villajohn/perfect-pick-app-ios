//
//  SingleItem.swift
//  PerfectPick
//
//  Created by Jhon Villalobos on 8/23/16.
//  Copyright © 2016 Jhon Villalobos. All rights reserved.
//

import Foundation

class SingleItem {
    var id: Int!
    var name: String!
    
    init(id: Int, name: String) {
        self.id = id
        self.name = name
    }
}
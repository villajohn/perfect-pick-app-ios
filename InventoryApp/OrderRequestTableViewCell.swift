//
//  OrderRequestTableViewCell.swift
//  PerfectPick
//
//  Created by Jhon Villalobos on 7/6/16.
//  Copyright © 2016 Jhon Villalobos. All rights reserved.
//

import UIKit

class OrderRequestTableViewCell: UITableViewCell {
    
    @IBOutlet weak var itemState: UISwitch!
    @IBOutlet weak var itemName: UILabel!
    @IBOutlet weak var itemBoxes: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

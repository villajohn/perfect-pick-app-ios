//
//  Status.swift
//  PerfectPick
//
//  Created by Jhon Villalobos on 7/7/16.
//  Copyright © 2016 Jhon Villalobos. All rights reserved.
//

import Foundation

class Status {
    
    struct StatusLibrary {
        
        let library = ["Pending": 1, "Revision": 2, "Loading": 3, "Transit": 4, "Completed": 5]
        
    }
    
}
//
//  MInventoryViewController.swift
//  PerfectPick
//
//  Created by Jhon Villalobos on 7/8/16.
//  Copyright © 2016 Jhon Villalobos. All rights reserved.
//

import UIKit
import SwiftSpinner
import Locksmith

class MInventoryViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITabBarDelegate {
    
    @IBOutlet weak var titleContainer: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tabBar: UITabBar!
    @IBOutlet weak var isSkidButton: UIButton!
    
    var refreshControl: UIRefreshControl!
    
    var isSkids = true
    
    var listItems   = [CollapsedProducts]()
    var productList = [Product]()
    
    var originRequest : Int?
    
    let searchController = UISearchController(searchResultsController: nil)
    var filteredItems    = [Product]()
    var itemsForFilter   = [Product]()

    override func viewDidLoad() {
        super.viewDidLoad()
        checkIfMarket()
        
        self.title = "Warehouse Inventory"
        self.tabBar.delegate = self
        
        self.titleContainer.backgroundColor = UIColor(colorLiteralRed: 60/255.0, green: 60/255.0, blue: 60/255.0, alpha: 1.0)
        self.titleContainer.layer.cornerRadius = 5.0
        self.tabBar.barTintColor = UIColor.black
        
        //Search functions
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        definesPresentationContext = true
        tableView.tableHeaderView = searchController.searchBar
        
        loadRefreshControl()
        getInventory()
        
    }
    
    func getInventory() {
        SwiftSpinner.show("loading...")
        let parameters = [] as AnyObject
        let methodCall = "\(URLServer)getInventoryAlphabetically"
        makeRequest(methodCall, metodo: .get, params: parameters) {
            response, error in
            if let ans = response {
                let content = ans["content"] as! [[String:AnyObject]]
                self.listItems.removeAll()
                self.itemsForFilter.removeAll()
                for prd in content {
                    self.productList.removeAll()
                    let newItem : CollapsedProducts = CollapsedProducts(categoryName: "", items: self.productList)
                    let categoryName = prd["name"] as! String
                    if let tal = prd["items"] {
                        let items = tal as! [[String:AnyObject]]
                        for e in items {
                            let itemId    = Int(e["items_id"] as! String)
                            let itemName  = e["description"] as! String
                            let itemPrice = Double(e["price"] as! String)
                            var itemQty = Double(0)
                            if let validQty = Double(e["quantity"] as! String) {
                                itemQty = validQty
                            }
                            let itemSkids = Int(e["skids"] as! String)
                            let totalBoxes = Int(e["total_boxes"] as! String)
                            let internList : Product = Product(id: itemId!, name: itemName, price: itemPrice!, quantity: itemQty, skids: itemSkids!, totalBoxes: totalBoxes!)
                            self.productList.append(internList)
                            self.itemsForFilter.append(internList)
                        }
                    } else {
                        let internList : Product = Product(id: 0, name: "", price: 0, quantity: 0, skids: 0, totalBoxes: 0)
                        self.productList.append(internList)
                    }
                    newItem.categoryName = categoryName
                    newItem.items =  self.productList
                    self.listItems.append(newItem)
                }
                self.do_cell_refresh()
            } else {
                SwiftSpinner.hide()
                globalMessage(APP_NAME as NSString, msgBody: NETWORK_ERROR as NSString, delegate: nil, self: self)
            }
        }
    }
    
    func do_cell_refresh() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.reloadData()
        SwiftSpinner.hide()
    }
    
    func loadRefreshControl() {
        refreshControl = UIRefreshControl()
        refreshControl.backgroundColor = UIColor.red
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to update data")
        refreshControl.addTarget(self, action: #selector(self.refreshTable), for: .valueChanged)
        self.tableView.addSubview(refreshControl)
    }
    
    func updateTable() {
        self.getInventory()
    }
    
    func refreshTable() {
        updateTable()
        refreshControl.endRefreshing()
    }
    
    //MARK: Table Delegates
    func numberOfSections(in tableView: UITableView) -> Int {
        if searchController.isActive && searchController.searchBar.text != "" {
            return 1
        }
        return listItems.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchController.isActive && searchController.searchBar.text != "" {
            return filteredItems.count
        }
        return (listItems[section].collapsed!) ? 0 : listItems[section].items.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableCell(withIdentifier: "MInventoryHeaderCell") as! MInventoryHeaderCell
        
        if searchController.isActive && searchController.searchBar.text != "" {
            header.titleButton.setTitle("Filtered List", for: UIControlState())
            return header.contentView
        }
        
        header.toggleButton.tag = section
        header.titleButton.tag = section
        header.titleButton.setTitle(listItems[section].categoryName, for: UIControlState())
        header.toggleButton.rotate(listItems[section].collapsed! ? 0.0 : CGFloat(Double.pi / 2))
        header.titleButton.addTarget(self, action: #selector(self.toggleCollapse(_:)), for: .touchUpInside)
        header.toggleButton.addTarget(self, action: #selector(self.toggleCollapse), for: .touchUpInside)
        
        return header.contentView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MInventoryCell") as! MInventoryTableViewCell
        
        if listItems.count > 0 {
            let dataCell: Product
            if searchController.isActive && searchController.searchBar.text != "" {
                dataCell = filteredItems[indexPath.row]
            } else {
                dataCell = listItems[indexPath.section].items[indexPath.row]
            }
            cell.itemName.text = dataCell.name
            switch isSkids {
            case false:
                isSkidButton.setTitle("# of Boxes", for: UIControlState())
                cell.itemBoxes.text = "\(dataCell.totalBoxes!)"
            default:
                isSkidButton.setTitle("# of Skids", for: UIControlState())
                cell.itemBoxes.text = "\(dataCell.quantity!)"
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50.0
    }
    
    func toggleCollapse(_ sender: UIButton) {
        /*
        let section = sender.tag
        let collapsed = listItems[section].collapsed
        
        // Toggle collapse
        listItems[section].collapsed = !collapsed
        
        // Reload section
        tableView.reloadSections(NSIndexSet(index: section), withRowAnimation: .Automatic)
 */
    }
    
    //MARK: Bottom Tabs actions
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        switch item.tag {
        case 1:
            let market = self.storyboard?.instantiateViewController(withIdentifier: "MMain") as! MMainViewController
            self.navigationController?.pushViewController(market, animated: true)
        case 2:
            let driver = self.storyboard?.instantiateViewController(withIdentifier: "MDriver") as! MDriverViewController
            self.navigationController?.pushViewController(driver, animated: true)
        default:
            return
        }
    }

    //*****

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func checkIfMarket() {
        var role = 0
        if let dictionary = Locksmith.loadDataForUserAccount(userAccount: "user") {
            role = dictionary["role"] as! Int
        }
        
        if role != 3 {
            deleteLogOut()
            let out = self.storyboard?.instantiateViewController(withIdentifier: "Home") as! HomeViewController
            self.navigationController?.pushViewController(out, animated: true)
        }
    }
    
    @IBAction func checkSkids(_ sender: AnyObject) {
         switch isSkids {
         case true:
         isSkids = false
         default:
         isSkids = true
         }
         self.do_cell_refresh()
    }
    
    //MARK: Search Functions
    func filterContentForSearchText(_ searchText: String, scope: String = "All") {
        filteredItems = itemsForFilter.filter { product in
            return product.name.lowercased().contains(searchText.lowercased())
        }
        tableView.reloadData()
    }
    
}

extension MInventoryViewController : UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        filterContentForSearchText(searchController.searchBar.text!)
    }
}

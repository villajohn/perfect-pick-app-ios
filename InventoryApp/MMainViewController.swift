//
//  MMainViewController.swift
//  PerfectPick
//
//  Created by Jhon Villalobos on 7/8/16.
//  Copyright © 2016 Jhon Villalobos. All rights reserved.
//

import UIKit
import Locksmith

class MMainViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UITabBarDelegate {
    
    @IBOutlet weak var titlesContainer: UIView!
    @IBOutlet weak var btnRequest: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tabBar: UITabBar!
    
    var refreshControl : UIRefreshControl!
    var list = [Product]()
    
    var originRequest : Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        checkIfMarket()
        
        self.title = "Market"
        self.titlesContainer.backgroundColor = UIColor(colorLiteralRed: 60/255.0, green: 60/255.0, blue: 60/255.0, alpha: 1.0)
        self.btnRequest.layer.cornerRadius = 5.0
        
        self.tabBar.delegate = self
        self.tabBar.barTintColor = UIColor.black
        
        loadRefreshControl()
        getInventory()
    }
    
    func updateTable() {
        self.list.removeAll()
        self.getInventory()
    }
    
    func loadRefreshControl() {
        refreshControl = UIRefreshControl()
        refreshControl.backgroundColor = UIColor.red
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to update data")
        refreshControl.addTarget(self, action: #selector(WMainViewController.refreshTable), for: .valueChanged)
        self.tableView.addSubview(refreshControl)
    }
    
    func refreshTable() {
        updateTable()
        refreshControl.endRefreshing()
    }
    
    //MARK: Bottom Tabs actions
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        switch item.tag {
        case 2:
            let driver = self.storyboard?.instantiateViewController(withIdentifier: "MDriver") as! MDriverViewController
            self.navigationController?.pushViewController(driver, animated: true)
        case 3:
            let warehouse = self.storyboard?.instantiateViewController(withIdentifier: "MInventory") as! MInventoryViewController
            self.navigationController?.pushViewController(warehouse, animated: true)
        default:
            return
        }
    }
    
    //MARK: Top tab buttons
    @IBAction func btnAddOrder(_ sender: AnyObject) {
        let makeOrder = self.storyboard?.instantiateViewController(withIdentifier: "MMakeOrder") as! MMakeOrderViewController
        self.navigationController?.pushViewController(makeOrder, animated: true)
    }
    
    @IBAction func btnLogout(_ sender: AnyObject) {
        deleteLogOut()
        let out = self.storyboard?.instantiateViewController(withIdentifier: "Home") as! HomeViewController
        self.navigationController?.pushViewController(out, animated: true)
    }
    
    func getInventory() {
        let parameters = ["key": API_KEY] as AnyObject
        let methodCall = "\(URLServer)getMarketOrder"
        makeRequest(methodCall, metodo: .get, params: parameters) {
            response, error in
            if let ans = response {
                self.list.removeAll()
                if ans["success"] as! Bool == true {
                    let content = ans["content"] as! [[String:AnyObject]]
                    for prd in content {
                        let itemId    = Int(prd["items_id"] as! String)
                        let itemName  = prd["name"] as! String
                        var itemQty = Double(0)
                        if let validQty = Double(prd["quantity"] as! String) {
                            itemQty = validQty
                        }
                        let itemSkids = Int(prd["skids"] as! String)
                        let totalBoxes = Int(prd["total_boxes"] as! String)
                        let internList : Product = Product(id: itemId!, name: itemName, price: 0.00, quantity: itemQty, skids: itemSkids!, totalBoxes: totalBoxes!)
                        self.list.append(internList)
                    }
                }
                self.do_cell_refresh()
            }
        }
    }
    
    // MARK : Table Delegates
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MMainCell", for: indexPath) as! MMainTableViewCell
        
        if list.count > 0 {
            let dataCell = list[indexPath.row]
            cell.itemName.text          = dataCell.name
            cell.itemBoxes.text   = "\(dataCell.quantity!)"
        }
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        var numSections = 0
        if (self.list.count > 0) {
            numSections = 1
        } else {
            let noDataLabel: UILabel = UILabel.init(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
            noDataLabel.text = "There no order available yet. \n Reasons Why? \n 1. You don't have an open order. \n 2. Driver has not yet loaded the order on the truck."
            noDataLabel.numberOfLines = 6
            noDataLabel.textColor = UIColor.red
            noDataLabel.textAlignment = .center
            tableView.backgroundView = noDataLabel
            tableView.separatorStyle = .none
        }
        return numSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func do_cell_refresh() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView?.reloadData()
        return
    }
    // ******
    
    @IBAction func buttonRequestDriver(_ sender: AnyObject) {
        let orderStatus = self.storyboard?.instantiateViewController(withIdentifier: "MOrderStatus") as! MOrderStatusViewController
        self.navigationController?.pushViewController(orderStatus, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func checkIfMarket() {
        var role = 0
        if let dictionary = Locksmith.loadDataForUserAccount(userAccount: "user") {
            role = dictionary["role"] as! Int
        }
        
        if role != 3 {
            deleteLogOut()
            let out = self.storyboard?.instantiateViewController(withIdentifier: "Home") as! HomeViewController
            self.navigationController?.pushViewController(out, animated: true)
        }
    }
    
}

//
//  WDriverViewController.swift
//  PerfectPick
//
//  Created by Jhon Villalobos on 7/7/16.
//  Copyright © 2016 Jhon Villalobos. All rights reserved.
//

import UIKit
import SwiftSpinner
import Locksmith

class WDriverViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UITabBarDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var titleTableContainer: UIView!
    @IBOutlet weak var tabBar: UITabBar!
    @IBOutlet weak var locationLabel: UILabel!
    
    var originRequest : Int?
    
    var refreshControl : UIRefreshControl!
    
    var transitList = [Transit]()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        checkIfWarehouse()
        
        self.title = "Driver Status"
        self.titleTableContainer.backgroundColor = UIColor(colorLiteralRed: 60/255.0, green: 60/255.0, blue: 60/255.0, alpha: 1.0)
        self.titleTableContainer.layer.cornerRadius = 5.0
        
        self.tabBar.delegate = self
        self.tabBar.barTintColor = UIColor.black
        
        loadRefreshControl()
        SwiftSpinner.show("loading...")
        self.getDriverOrder()
        SwiftSpinner.hide()
    }
    
    //MARK: TABLE DELEGATES
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(select)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        var numSections = 0
        if (self.transitList.count > 0) {
            numSections = 1
        } else {
            let noDataLabel: UILabel = UILabel.init(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
            noDataLabel.text = "There no order available yet. \n Reasons Why? \n 1. Market never made an order. \n 2. Market has yet to send order request to you."
            noDataLabel.numberOfLines = 6
            noDataLabel.textColor = UIColor.red
            noDataLabel.textAlignment = .center
            tableView.backgroundView = noDataLabel
            tableView.separatorStyle = .none
        }
        return numSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.transitList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WDriver") as! WDriverTableViewCell
        
        let items = self.transitList[indexPath.row]
        cell.itemName.text  = items.itemName
        cell.itemBoxes.text = "\(items.requested)"
        
        return cell
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func loadRefreshControl() {
        refreshControl = UIRefreshControl()
        refreshControl.backgroundColor = UIColor.red
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to update data")
        refreshControl.addTarget(self, action: #selector(WDriverViewController.refreshTable), for: .valueChanged)
        self.tableView.addSubview(refreshControl)
    }
    
    func refreshTable() {
        getDriverOrder()
        refreshControl.endRefreshing()
    }
    
    //MAR: Network functions
    func getDriverOrder() {
        let parameters = ["key": API_KEY] as AnyObject
        let methodCall = "\(URLServer)getDriverOrder"
        makeRequest(methodCall, metodo: .get, params: parameters) {
            response, error in
            if let ans = response {
                self.transitList.removeAll()
                if ans["success"] as! Bool == true {
                    let content = ans["content"] as! [[String: AnyObject]]
                    for prd in content {
                        self.locationLabel.text = prd["location"] as? String
                        let recordId  = Int(prd["id"] as! String)!
                        let requested = Int(prd["requested"] as! String)!
                        let quantity  = Int(prd["quantity"] as! String)!
                        let destiny   = Int(prd["destinationLocationId"] as! String)!
                        let pick      = Int(prd["pickupLocationId"] as! String)!
                        let confirm   = Int(prd["confirmed"] as! String)!
                        let item      = Int(prd["items_id"] as! String)!
                        let truck     = Int(prd["trucks_id"] as! String)!
                        let market    = Int(prd["users_id"] as! String)!
                        let itemName  = prd["description"] as! String
                        let localList : Transit = Transit(id: recordId, request: requested, qty: quantity, dest: destiny, pick: pick, confirm: confirm, item: item, truck: truck, market: market, name: itemName)
                        self.transitList.append(localList)
                    }
                } else {
                    //self.showMessage(ans["message"] as! String)
                }
                self.do_cell_refresh()
            } else {
                self.showMessage(NETWORK_ERROR)
            }
        }
    }
    
    func do_cell_refresh() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView?.reloadData()
        return
    }
    
    //MARK: Alerts
    func showMessage(_ message: String) {
        let alert = UIAlertController(title: APP_NAME, message: message, preferredStyle: .alert)
        let cancel = UIAlertAction(title: "Close", style: .cancel, handler: { (action) -> Void in })
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK: Bottom Tabs actions
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        switch item.tag {
        case 1:
            if originRequest == 1 {
                let main = self.storyboard?.instantiateViewController(withIdentifier: "WMain") as! WMainViewController
                self.navigationController?.pushViewController(main, animated: true)
            } else if originRequest == 3 {
                let marketInventory = self.storyboard?.instantiateViewController(withIdentifier: "MInventory") as! MInventoryViewController!
                self.navigationController?.pushViewController(marketInventory!, animated: true)
            }
        case 3:
            if originRequest == 1 {
            let market = self.storyboard?.instantiateViewController(withIdentifier: "WMarket") as! WMarketViewController
            self.navigationController?.pushViewController(market, animated: true)
            } else if originRequest == 3 {
                let mainMarket = self.storyboard?.instantiateViewController(withIdentifier: "MMain") as! MMainViewController!
                self.navigationController?.pushViewController(mainMarket!, animated: true)
            }
        default:
            return
        }
    }
    
    func checkIfWarehouse() {
        var role = 0
        if let dictionary = Locksmith.loadDataForUserAccount(userAccount: "user") {
            role = dictionary["role"] as! Int
        }
        
        if role != 1 {
            deleteLogOut()
            let out = self.storyboard?.instantiateViewController(withIdentifier: "Home") as! HomeViewController
            self.navigationController?.pushViewController(out, animated: true)
        }
    }
    


}

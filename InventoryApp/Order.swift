//
//  Order.swift
//  PerfectPick
//
//  Created by Jhon Villalobos on 7/6/16.
//  Copyright © 2016 Jhon Villalobos. All rights reserved.
//

import Foundation

class Order {
    var id: Int!
    var qty: Int!
    var qtyConfirmed: Int!
    var date: Date!
    var locationId: Int!
    var itemId: Int!
    var marketId: Int!
    var itemName: String!
    var status: Bool!
    var orderId: Int!
    var locationName: String!
    
    init(id: Int, qty: Int, qtyConfirmed: Int, date: Date, locId: Int, itemId: Int, marketId: Int, itemName: String, status: Bool, orderId: Int!, locationName: String!) {
        self.id           = id
        self.qty          = qty
        self.qtyConfirmed = qtyConfirmed
        self.date         = date
        self.locationId   = locId
        self.itemId       = itemId
        self.marketId     = marketId
        self.itemName     = itemName
        self.status       = status
        self.orderId      = orderId
        self.locationName = locationName
    }
}

//
//  ScannerViewController.swift
//  PerfectPick
//
//  Created by Jhon Villalobos on 2/1/17.
//  Copyright © 2017 Jhon Villalobos. All rights reserved.
//

import AVFoundation
import UIKit
import SwiftSpinner

class ScannerViewController: UIViewController, AVCaptureMetadataOutputObjectsDelegate, UIPickerViewDelegate {

    var captureSession: AVCaptureSession!
    var previewLayer: AVCaptureVideoPreviewLayer!
    var qrCodeFrameView:UIView?
    let screenSize = UIScreen.main.bounds
    let metadataOutput = AVCaptureMetadataOutput()
    let squareScan = CALayer()
    
    var isScaning = false
    
    // 1 -> Inventory -/- 2 -> Create Order
    var kindScreen: Int!
    var isAdding: Bool! //Check if TRUE: Receive (Add Inventory) or FALSE: Substract from inventory
    
    //picker
    let pickerCategory     = UIPickerView()
    var categoryDetails    = [SingleItem]()
    var categorySelected   : SingleItem = SingleItem(id: 0, name: "")
    
    var categoryToSend = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.black
        captureSession = AVCaptureSession()
        
        let videoCaptureDevice = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
        let videoInput: AVCaptureDeviceInput
        
        do {
            videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
        } catch {
            return
        }
        
        if (captureSession.canAddInput(videoInput)) {
            captureSession.addInput(videoInput)
        } else {
            failed();
            return;
        }
        
        //let metadataOutput = AVCaptureMetadataOutput()
    
        if (captureSession.canAddOutput(metadataOutput)) {
            captureSession.addOutput(metadataOutput)
            
            metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            metadataOutput.metadataObjectTypes = [AVMetadataObjectTypeQRCode]
        } else {
            failed()
            return
        }
        
        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession);
        previewLayer.frame = view.layer.bounds //CGRect(x: screenSize.width * 0.25, y: screenSize.height * 0.25, width: 200, height: 200) //view.layer.bounds
        
        previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
        
        view.layer.addSublayer(previewLayer)
        view.isOpaque = true
        view.backgroundColor = UIColor(colorLiteralRed: 237/255.0, green: 237/255.0, blue: 237/255.0, alpha: 0.8)
        
        captureSession.startRunning();
        
        // Initialize QR Code Frame to highlight the QR code
        qrCodeFrameView = UIView()
        qrCodeFrameView?.frame = view.bounds //CGRect(x: screenSize.width * 0.25, y: screenSize.height * 0.25, width: 200, height: 200)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.avCaptureInputPortFormatDescriptionDidChangeNotification(notification:)), name:NSNotification.Name.AVCaptureInputPortFormatDescriptionDidChange, object: nil)
        
//        let rect = previewLayer.rectForMetadataOutputRect(ofInterest: CGRect(x: screenSize.width * 0.25, y: screenSize.height * 0.25, width: 200, height: 200))
//        
//        DispatchQueue.main.async {
//            //self.qrCodeFrameView?.frame = rect
//        }
        
        if let qrCodeFrameView = qrCodeFrameView {
            
            squareScan.borderColor = UIColor.red.cgColor
            squareScan.borderWidth = CGFloat(2)
            squareScan.frame = CGRect(x: screenSize.width * 0.40, y: screenSize.height * 0.40, width: 100, height: 100)
            qrCodeFrameView.layer.addSublayer(squareScan)
            
            qrCodeFrameView.layer.borderColor = UIColor(white: 1.0, alpha: 0.7).cgColor //UIColor(colorLiteralRed: 114/255.0, green: 113/255.0, blue: 118/255.0, alpha: 1.0).cgColor
            qrCodeFrameView.layer.borderWidth = 1
            
            //top border
            let borderTop = CALayer()
            borderTop.borderColor = UIColor(white: 1.0, alpha: 0.7).cgColor
            borderTop.borderWidth = CGFloat(100)
            borderTop.frame = CGRect(x: 0, y: 0, width: screenSize.width, height: 200)
            qrCodeFrameView.layer.addSublayer(borderTop)
            qrCodeFrameView.layer.masksToBounds = true
            
            //bottom border
            let borderBottom = CALayer()
            borderBottom.borderColor = UIColor(white: 1.0, alpha: 0.7).cgColor
            borderBottom.borderWidth = CGFloat(100)
            borderBottom.frame = CGRect(x: 0, y: screenSize.height * 0.70, width: screenSize.width, height: 200)
            borderBottom.borderWidth = CGFloat(100)
            qrCodeFrameView.layer.addSublayer(borderBottom)
            qrCodeFrameView.layer.masksToBounds = true
            
            //left border
            let borderLeft = CALayer()
            borderLeft.borderColor = UIColor(white: 1.0, alpha: 0.7).cgColor
            borderLeft.borderWidth = CGFloat(50)
            borderLeft.frame = CGRect(x: 0, y: 200, width: 50, height: screenSize.height - 400)
            qrCodeFrameView.layer.addSublayer(borderLeft)
            qrCodeFrameView.layer.masksToBounds = true
            
            //right border
            let borderRight = CALayer()
            borderRight.borderColor = UIColor(white: 1.0, alpha: 0.7).cgColor
            borderRight.borderWidth = CGFloat(50)
            borderRight.frame = CGRect(x: screenSize.width * 0.88, y: 200, width: 50, height: screenSize.height - 400)
            qrCodeFrameView.layer.addSublayer(borderRight)
            qrCodeFrameView.layer.masksToBounds = true
            
            view.addSubview(qrCodeFrameView)
            view.bringSubview(toFront: qrCodeFrameView)
            
            let backFrame = UIView()
            backFrame.frame = view.layer.bounds
            backFrame.backgroundColor = UIColor(white: 1.0, alpha: 0.7)
            view.bringSubview(toFront: backFrame)
            
            
            let scanButton = UIButton()
            scanButton.translatesAutoresizingMaskIntoConstraints = false
            scanButton.setTitle("SCAN", for: .normal)
            scanButton.backgroundColor = UIColor.red
            scanButton.setTitleColor(UIColor.white, for: .normal)
            scanButton.layer.cornerRadius = 5.0
            scanButton.addTarget(self, action: #selector(self.activateScanFunction), for: .touchDown)
            scanButton.addTarget(self, action: #selector(self.releaseScanFunction), for: .touchUpInside)
            view.addSubview(scanButton)
            NSLayoutConstraint.activate([
                scanButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 10),
                scanButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -10),
                scanButton.heightAnchor.constraint(equalToConstant: 45),
                scanButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -10)
                ])
        }
        
        if kindScreen == 2 {
            showListButton()
        }
        
        if kindScreen == 1 {
            createAddButton()
            if isAdding == true {
                self.title = "RECEIVING"
            } else {
                self.title = "SHIPPING"
            }
        }
    }
    
    func activateScanFunction() {
        squareScan.borderColor = UIColor.green.cgColor
        isScaning = true
    }
    
    func releaseScanFunction() {
        isScaning = false
        squareScan.borderColor = UIColor.red.cgColor
    }
    
    func avCaptureInputPortFormatDescriptionDidChangeNotification(notification: NSNotification) {
        
        let scanRect = CGRect(x: screenSize.width * 0.40, y: screenSize.height * 0.40, width: 100, height: 100)
        metadataOutput.rectOfInterest = previewLayer.metadataOutputRectOfInterest(for: scanRect) //pre.metadataOutputRectOfInterestForRect(scanRect)
        
    }
    
    func createAddButton() {
        let addButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(self.showNewItemWindow))
        self.navigationItem.rightBarButtonItem = addButton
    }
    
    func showListButton() {
        let buttonList = UIButton()
        buttonList.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(buttonList)
        NSLayoutConstraint.activate([
            buttonList.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 10),
            buttonList.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -10),
            buttonList.heightAnchor.constraint(equalToConstant: 35),
            buttonList.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 5)
            ])
    }
    
    func failed() {
        let ac = UIAlertController(title: "Scanning not supported", message: "Your device does not support scanning a code from an item. Please use a device with a camera.", preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default))
        present(ac, animated: true)
        captureSession = nil
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if (captureSession?.isRunning == true) {
            captureSession.stopRunning();
        }
    }
    
    func captureOutput(_ captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [Any]!, from connection: AVCaptureConnection!) {
        
        if isScaning == true {
            
            captureSession.stopRunning()
            
            if let metadataObject = metadataObjects.first {
                let readableObject = metadataObject as! AVMetadataMachineReadableCodeObject;
                
                AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))

                found(code: readableObject.stringValue);
            }
            
            dismiss(animated: true)
        }
    }
    
    func found(code: String) {
        let url = "\(URLServer)getItemById"
        let parameters = ["id": code] as AnyObject
        makeRequest(url, metodo: .get, params: parameters) {
            response, error in
            if let ans = response {
                if ans["success"] as! Bool == true {
                    let product = Product.populateProduct(data: ans)
                    let screenSelected = self.kindScreen!
                    switch screenSelected {
                        case 1:
                            self.showEditWindow(product.name!, qty: Int(product.quantity!), itemId: product.id!, itemPrice: product.price!, boxes: product.totalBoxes!)
                        case 2:
                            self.showOrderWindow(product.name!, qty: Int(product.quantity!), itemId: product.id!, itemPrice: product.price!, boxes: product.totalBoxes!)
                        default:
                            self.showEditWindow(product.name!, qty: Int(product.quantity!), itemId: product.id!, itemPrice: product.price!, boxes: product.totalBoxes!)
                    }
                    
                } else {
                    self.showNewItemWindow()
                }
            }
        }
    }
    
    func showNewItemWindow() {
        let alert = UIAlertController(title: APP_NAME, message: "New Item", preferredStyle: .alert)
        alert.addTextField(configurationHandler: configurationItemField)
        alert.addTextField(configurationHandler: configurationBoxField)
        alert.addTextField(configurationHandler: configurationSkidsField)
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (action) -> Void in
            self.captureSession.startRunning()
        })
        alert.addAction(cancel)
        let create = UIAlertAction(title: "Create", style: .default, handler: {
            (action) -> Void in
            if self.itemTxField.text?.isEmpty == false && Int(self.boxesTxField.text!) != nil {
                SwiftSpinner.show("saving...")
                self.updateItem(self.itemTxField.text!, itemQty: Int(self.boxesTxField.text!)!, itemId: 0, itemPrice: 0.00, skids: Int(self.skidsTxField.text!)!, category: self.categoryToSend)
                SwiftSpinner.hide()
            } else {
                globalMessage(APP_NAME as NSString, msgBody: "Both fields are mandatory and Numb. of boxes must be a number", delegate: nil, self: self)
            }
        })
        alert.addAction(create)
        self.present(alert, animated: true, completion: nil)
    }

    
    var itemTxField: UITextField!
    var categoryTxField: UITextField!
    var boxesTxField: UITextField!
    var skidsTxField: UITextField!
    func showEditWindow(_ name: String, qty: Int, itemId: Int, itemPrice: Double, boxes: Int) {
        let alert = UIAlertController(title: APP_NAME, message: "Edit Item",
                                      preferredStyle: .alert)
        alert.addTextField(configurationHandler: configurationItemField)
        alert.addTextField(configurationHandler: configurationBoxField)
        itemTxField.text  = name
        itemTxField.isEnabled = false
        //boxesTxField.text = "\(boxes)"
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) -> Void in
            self.captureSession.startRunning()
        })
        alert.addAction(cancel)
        let doIt = UIAlertAction(title: "Ok", style: .default, handler: {
            (action) -> Void in
            SwiftSpinner.show("editing...")
            if Int(self.boxesTxField.text!) != nil {
                let qty = (self.isAdding == true ? Int(self.boxesTxField.text!)! : Int(self.boxesTxField.text!)! * -1)
                self.updateItem(self.itemTxField.text!, itemQty: qty, itemId: itemId, itemPrice: itemPrice, skids: qty, category: 0)
            } else {
                globalMessage(APP_NAME as NSString, msgBody: "The field for boxes must be a number", delegate: nil, self: self)
            }
            SwiftSpinner.hide()
        })
        alert.addAction(doIt)
        self.present(alert, animated: true, completion: nil)
    }
    
    func showOrderWindow(_ name: String, qty: Int, itemId: Int, itemPrice: Double, boxes: Int) {
        let alert = UIAlertController(title: APP_NAME, message: "Please include the number of Boxes",
                                      preferredStyle: .alert)
        alert.addTextField(configurationHandler: configurationBoxField)

        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) -> Void in })
        alert.addAction(cancel)
        let doIt = UIAlertAction(title: "Ok", style: .default, handler: {
            (action) -> Void in
            SwiftSpinner.show("editing...")
            if Int(self.boxesTxField.text!) != nil {
                let qtyRequest = Int(self.boxesTxField.text!)!
                let saved = saveModel(id: itemId, qty: qtyRequest)
                if !saved {
                    globalMessage(APP_NAME as NSString, msgBody: "Can't save the order", delegate: nil, self: self)
                }
                self.captureSession.startRunning()
            } else {
                globalMessage(APP_NAME as NSString, msgBody: "The field for boxes must be a number", delegate: nil, self: self)
            }
            SwiftSpinner.hide()
        })
        alert.addAction(doIt)
        self.present(alert, animated: true, completion: nil)
    }
    
    func configurationItemField(_ textField: UITextField!) {
        textField.placeholder = "Item Name"
        itemTxField = textField
    }
    
    func configurationCategoryField(_ textField: UITextField!) {
        textField.placeholder = "Select Category"
        pickerCategory.delegate = self
        pickerCategory.showsSelectionIndicator = true
        categoryTxField = textField
        pickerButtons(categoryTxField)
    }
    
    func configurationBoxField(_ textField: UITextField!) {
        textField.placeholder = "Numb. Skids"
        textField.keyboardType = .numberPad
        boxesTxField = textField
    }
    
    func configurationSkidsField(_ textField: UITextField!) {
        textField.placeholder = "Boxes per Skid"
        textField.keyboardType = .numberPad
        skidsTxField = textField
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    func pickerButtons(_ sender: UITextField) {
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 59/255, green: 89/255, blue: 152/255, alpha: 1)
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Ok", style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.donePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        
        toolBar.setItems([spaceButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        categoryTxField.inputView = pickerCategory
        
        sender.inputAccessoryView = toolBar
    }
    
    func donePicker(_ sender: UITextField) {
        self.categoryTxField.endEditing(true)
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    func updateItem(_ itemName: String, itemQty: Int, itemId: Int, itemPrice: Double, skids: Int, category: Int) {
        let parameters = ["key": API_KEY, "id": itemId, "user": userGlobal, "description": itemName, "quantity": itemQty, "price": itemPrice, "skids": skids, "category": category] as AnyObject
        let methodCall = "\(URLServer)editItemScannerVersion"
        makeRequest(methodCall, metodo: .post, params: parameters) {
            response, error in
            if let ans = response {
                if ans["success"] as! Bool == true {
                    SwiftSpinner.show("saving...")
                    self.captureSession.startRunning()
                    SwiftSpinner.hide()
                } else {
                    globalMessage(APP_NAME as NSString, msgBody: ans["message"] as! NSString, delegate: nil, self: self)
                }
            }
        }
    }
    
    //MARK: Picker Delegate Methods
    func numberOfComponentsInPickerView(_ pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.categoryDetails[row].name
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.categoryDetails.count
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        categoryToSend = self.categoryDetails[row].id
        self.categoryTxField.text = self.categoryDetails[row].name
    }

}

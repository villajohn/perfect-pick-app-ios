//
//  DMainViewController.swift
//  PerfectPick
//
//  Created by Jhon Villalobos on 7/8/16.
//  Copyright © 2016 Jhon Villalobos. All rights reserved.
//

import UIKit
import SwiftSpinner
import Locksmith

class DMainViewController: UIViewController, UITabBarDelegate, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var btnSend: UIButton!
    @IBOutlet weak var titleContainer: UIView!
    @IBOutlet weak var tabBar: UITabBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var locationLabel: UILabel!
    
    var orderList = [Order]()
    
    var idsToSend  = [Int: Int]()
    var qtyOrdered = [Int: Int]()
    
    //values to send API
    var arrayIds = [Int]()
    var arrayQty = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        checkIfDriver()

        self.title = "Driver Page"
        
        self.tabBar.barTintColor = UIColor.black
        self.tabBar.delegate = self
        
        
        self.titleContainer.tintColor = UIColor.white
        self.titleContainer.backgroundColor = UIColor(colorLiteralRed: 60/255.0, green: 60/255.0, blue: 60/255.0, alpha: 1.0)
        self.titleContainer.layer.cornerRadius = 5.0
        
        self.btnSend.layer.cornerRadius = 5.0
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(DMainViewController.hideKeyboardd))
        self.tableView.addGestureRecognizer(tap)
        
        self.getOrder()
        
    }
    
    func hideKeyboardd(_ sender: UITapGestureRecognizer) {
        view.endEditing(true)
    }
    
    func updateQtySelected(_ sender:UITextField) {
        if Int(sender.text!) == nil {
            sender.text = ""
            self.showMessage("You must enter a numeric value")
        } else {
            if self.checkQtyVsRequest(Int(sender.text!)!, row: sender.tag) == false {
                sender.text = "\(String(describing: self.qtyOrdered[sender.tag]))"
            } else {
                self.qtyOrdered[sender.tag] = Int(sender.text!)
            }
        }
    }
    
    //Check if the requested quantity is not more than the inventory
    func checkQtyVsRequest(_ requested: Int, row: Int) -> Bool {
        var answer = true
        if self.orderList.count > 0 {
            let itemQty = orderList[row].qty
            if requested > itemQty! {
                showMessage("You can't request more boxes than \(String(describing: itemQty))")
                answer = false
            }
        }
        return answer
    }
    
    // MARK: TableView Delegates
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DMainCell") as! DMainTableViewCell
        
        let itemRow = self.orderList[indexPath.row]
        cell.itemName.text  = itemRow.itemName
        cell.itemBoxes.tag = indexPath.row
        cell.itemBoxes.addTarget(self, action: #selector(self.updateQtySelected(_:)), for: .editingDidEnd)
        cell.itemBoxes.text = "\(String(describing: itemRow.qty!))"
        cell.itemBoxes.delegate = self
        cell.itemBoxes.keyboardType = .numberPad
        
        if let cellText = qtyOrdered[indexPath.row] {
            cell.itemBoxes.text = "\(cellText)"
        } else {
            cell.itemBoxes.text = ""
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        var numSections = 0
        if (self.orderList.count > 0) {
            numSections = 1
        } else {
            let noDataLabel: UILabel = UILabel.init(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
            noDataLabel.text = "There no order available yet. \n Reasons Why? \n 1. Market never made an order. \n 2. Warehouse has yet to send order request to you."
            noDataLabel.numberOfLines = 6
            noDataLabel.textColor = UIColor.red
            noDataLabel.textAlignment = .center
            tableView.backgroundView = noDataLabel
            tableView.separatorStyle = .none
        }
        return numSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.orderList.count
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
//        let delete = UITableViewRowAction(style: UITableViewRowActionStyle(), title: "Delete") { (action, indexPath) in
//            let product = self.orderList[indexPath.row]
//            self.deleteRequest(product.itemId)
//        }
        
        return nil
    }
    
    //MARK: Network request
    func getOrder() {
        let orderStatus = Status.StatusLibrary().library["Loading"]
        let parameters = ["key": API_KEY, "status": orderStatus!] as AnyObject
        let methodCall = "\(URLServer)getOrder"
        makeRequest(methodCall, metodo: .get, params: parameters) {
            response, error in
            if let ans = response {
                if ans["success"] as! Bool == true {
                    let content = ans["content"] as! [[String:AnyObject]]
                    self.orderList.removeAll()
                    if content.count > 0 {
                        self.btnSend.isEnabled = true
                        var i = 0
                        for prd in content {
                            let idRow    = Int(prd["id"] as! String)!
                            let qtyItem  = Int(prd["quantity"] as! String)!
                            let qtyConfirmed = Int(prd["quantity"] as! String)!
                            let dateItem = convertStringDate(prd["date"] as! String)
                            let locat    = Int(prd["locations_id"] as! String)!
                            let itId     = Int(prd["items_id"] as! String)!
                            let userId   = Int(prd["users_id"] as! String)!
                            let itemName = prd["name"] as! String
                            let orderId  = Int(prd["orders_id"] as! String)!
                            let itemStatus = Int(prd["confirmed"] as! String)
                            let locationName = prd["location"] as! String
                            let switchStatus = (itemStatus == 1 ? true : false)
                            
                            self.qtyOrdered[i] = qtyItem
                            self.idsToSend[i]  = itId
                            
                            let fillingList : Order = Order(id: idRow, qty: qtyItem, qtyConfirmed: qtyConfirmed, date: dateItem, locId: locat, itemId: itId, marketId: userId, itemName: itemName, status: switchStatus, orderId: orderId, locationName: locationName)
                            self.orderList.append(fillingList)
                            self.locationLabel.text = locationName
                            i += 1
                        }
                    } else {
                        self.btnSend.isEnabled = false
                        //self.showMessage("No pending orders")
                    }
                    self.do_cell_refresh()
                } else {
                    //self.showMessage(ans["message"] as! String)
                }
            } else {
                self.showMessage(NETWORK_ERROR)
            }
        }
    }
    
    func deleteRequest(_ id: Int) {
        let orderId = self.orderList[0].orderId
        let alert = UIAlertController(title: APP_NAME, message: "Are you sure you want to delete this product?", preferredStyle: .alert)
        let cancel = UIAlertAction(title: "No", style: .cancel, handler: { (action) -> Void in })
        let doIt = UIAlertAction(title: "Yes", style: .destructive, handler: { (action) -> Void in
            let parameters = ["key": API_KEY, "itemId": id, "user": userGlobal, "order": orderId! ] as AnyObject
            let methodCall = "\(URLServer)deleteItemOrdered"
            makeRequest(methodCall, metodo: .post, params: parameters) {
                response, error in
                if let ans = response {
                    if ans["success"] as! Bool == true {
                        SwiftSpinner.show("updating...")
                        self.updateTable()
                        SwiftSpinner.hide()
                    } else {
                        self.showMessage(ans["message"] as! String)
                    }
                }
            }
        })
        alert.addAction(cancel)
        alert.addAction(doIt)
        self.present(alert, animated: true, completion: nil)
    }
    
    func do_cell_refresh() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView?.reloadData()
        return
    }
    
    func updateTable() {
        self.orderList.removeAll()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.getOrder()
    }
    
    func showMessage(_ message: String) {
        let alert = UIAlertController(title: APP_NAME, message: message, preferredStyle: .alert)
        let cancel = UIAlertAction(title: "Close", style: .cancel, handler: { (action) -> Void in })
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func btnSendToMarket(_ sender: AnyObject) {
        let alert = UIAlertController(title: APP_NAME, message: "Are you sure you want to send the order to the Market ?", preferredStyle: .alert)
        let cancel = UIAlertAction(title: "No", style: .cancel, handler: { (action) -> Void in })
        let doIt   = UIAlertAction(title: "Yes", style: .default, handler: {
            (action) -> Void in
            self.sendToMarket()
        })
        alert.addAction(cancel)
        alert.addAction(doIt)
        self.present(alert, animated: true, completion: nil)
    }
    
    func sendToMarket() {
        self.btnSend.isEnabled =  false
        SwiftSpinner.show("saving the order...")
        let totalSelected = idsToSend.count
        arrayIds.removeAll()
        arrayQty.removeAll()
        if totalSelected == 0 {
            self.btnSend.isEnabled = true
            self.showMessage("You must select at least 1 item")
        }
        
        var i = 0
        for (position, _) in idsToSend {
            arrayIds.append(idsToSend[position]!)
            arrayQty.append("\(qtyOrdered[position]!)")
            i += 1
            if i == totalSelected {
                let orderId = self.orderList[0].orderId
                self.processOrder(self.arrayIds as NSArray, itemsQty: self.arrayQty as NSArray, orderId: orderId!)
            }
        }
        SwiftSpinner.hide()
    }
    
    //func processOrder(item: Order) {
    func processOrder(_ itemsId: NSArray, itemsQty: NSArray, orderId: Int) {
        //let parameters = ["key": API_KEY, "user": userGlobal, "itemId": item.itemId, "qty": item.qtyConfirmed, "order": item.orderId] as AnyObject
        let parameters = ["key": API_KEY, "user": userGlobal, "itemId": itemsId, "qty": itemsQty, "order": orderId] as AnyObject
        let methodCall = "\(URLServer)processDriverOrder"
        makeRequest(methodCall, metodo: .post, params: parameters) {
            response, error in
            if let ans = response {
                if ans["success"] as! Bool == true {
                    self.orderList.removeAll()
                    self.do_cell_refresh()
                } else {
                    self.btnSend.isEnabled = true
                    self.showMessage(ans["message"] as! String)
                }
            } else {
                self.btnSend.isEnabled = true
                self.showMessage(NETWORK_ERROR)
            }
        }
    }
    
    //MARK: Bottom Tabs actions
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {        switch item.tag {
        case 2:
            let driver = self.storyboard?.instantiateViewController(withIdentifier: "DInventory") as! DInventoryViewController
            self.navigationController?.pushViewController(driver, animated: true)
        case 3:
            let warehouse = self.storyboard?.instantiateViewController(withIdentifier: "DMarket") as! DMarketViewController
            self.navigationController?.pushViewController(warehouse, animated: true)
        default:
            return
        }
    }
    
    
    @IBAction func btnLogOut(_ sender: AnyObject) {
        deleteLogOut()
        let logOut = self.storyboard?.instantiateViewController(withIdentifier: "Home") as!HomeViewController
        self.navigationController?.pushViewController(logOut, animated: true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool // called when 'return' key pressed. return NO to ignore.
    {
        textField.resignFirstResponder()
        return true;
    }
    
    func checkIfDriver() {
        var role = 0
        if let dictionary = Locksmith.loadDataForUserAccount(userAccount: "user") {
            role = dictionary["role"] as! Int
        }
        
        if role != 2 {
            deleteLogOut()
            let out = self.storyboard?.instantiateViewController(withIdentifier: "Home") as! HomeViewController
            self.navigationController?.pushViewController(out, animated: true)
        }
    }
    

}

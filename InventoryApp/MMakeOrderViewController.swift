//
//  MMakeOrderViewController.swift
//  PerfectPick
//
//  Created by Jhon Villalobos on 7/8/16.
//  Copyright © 2016 Jhon Villalobos. All rights reserved.
//

import UIKit
import SwiftSpinner
import Locksmith

class MMakeOrderViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITabBarDelegate {
    
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var titleContainer: UIView!
    @IBOutlet weak var tabBar: UITabBar!
    @IBOutlet weak var tableView: UITableView!
    
    var productList = [Product]()
    var preparedList = [Product]() //Used for prepare an order and send it to server
    var productsSelected = [ProductOrder]()
    
    var itemsToSend  = [Int:String]()
    var idsToSend    = [Int: Int]()
    var switchToSend = [Int:Bool]()
    var itemsOrdered = [Int: Int]()
    
    //search bar
    let searchController = UISearchController(searchResultsController: nil)
    var filteredItems = [Product]()
    
    //values to send API
    var arrayIds = [Int]()
    var arrayQty = [String]()

    override func viewDidLoad() {
        super.viewDidLoad()
        checkIfMarket()
        self.title = "Place New Order"
        
        self.tabBar.delegate = self
        self.tabBar.barTintColor = UIColor.black
        
        self.titleContainer.backgroundColor = UIColor(colorLiteralRed: 60/255.0, green: 60/255.0, blue: 60/255.0, alpha: 1.0)
        self.titleContainer.layer.cornerRadius = 5.0
        self.btnSubmit.layer.cornerRadius = 5.0
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(MMakeOrderViewController.hideKeyboardd))
        self.tableView.addGestureRecognizer(tap)
        
        //Search functions
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        definesPresentationContext = true
        tableView.tableHeaderView = searchController.searchBar
        
        checkOpenOrder()
    }
    
    func hideKeyboardd(_ sender: UITapGestureRecognizer) {
        view.endEditing(true)
    }
    
    //Receive the switch action from TableView
    func changeItemStatus(_ sender:UISwitch) {
        //let numberRow = sender.tag
        //let indexPath = NSIndexPath(forRow: numberRow, inSection:0)
        //let cell : MMakeOrderTableViewCell? = self.tableView.cellForRowAtIndexPath(indexPath) as! MMakeOrderTableViewCell?
        switchToSend[sender.tag] = sender.isOn
    }
    
    func updateQtySelected(_ sender:UITextField) {
        if Double(sender.text!) == nil {
            if sender.text?.isEmpty == false {
                sender.text = ""
                self.showMessage("You must enter a numeric value")
            }
        } else {
            if self.checkQtyVsInventory(Double(sender.text!)!, row: sender.tag) == false {
                sender.text = ""
            } else {
                let tagRow          = sender.tag
                itemsToSend[tagRow] = sender.text
                let totalItems = self.productList.count
                for i in 0 ..< totalItems {
                    if self.productList[i].id == sender.tag {
                        idsToSend[tagRow] = self.productList[i].id
                        break
                    }
                }
            }
        }
    }
    
    //Check if the requested quantity is not more than the inventory
    func checkQtyVsInventory(_ requested: Double, row: Int) -> Bool {
        let answer = true
        if self.productList.count > 0 {
            
            let totalProducts = self.productList.count
            for i in 0 ..< totalProducts {
                if self.productList[i].id == row {
                    let itemQty = productList[i].quantity
                    if requested > itemQty! {
                        showMessage("You can't request more boxes than \(itemQty!)")
                        return false
                    }
                }
            }
        }
        return answer
    }
    
    @IBAction func btnSubmitOrder(_ sender: AnyObject) {
        let alert = UIAlertController(title: APP_NAME, message: "Are you sure to send this order ?", preferredStyle: .alert)
        let cancel = UIAlertAction(title: "No", style: .cancel, handler: { (action) -> Void in })
        let doIt   = UIAlertAction(title: "Yes", style: .default, handler: { (action) -> Void in
            self.confirmSubmitOrder()
        })
        alert.addAction(cancel)
        alert.addAction(doIt)
        self.present(alert, animated: true, completion: nil)
    }
    
    func confirmSubmitOrder() {
        SwiftSpinner.show("saving the order...")
        let totalSelected = switchToSend.count
        arrayIds.removeAll()
        arrayQty.removeAll()
        if totalSelected == 0 {
            self.showMessage("You must select at least 1 item")
        }
        
        var i = 0;
        for (position, _) in itemsToSend {
            //arrayIds.append(idsToSend[position]!)
            arrayIds.append(position)
            arrayQty.append(itemsToSend[position]!)
            i += 1
            if i == totalSelected {
                self.createNewOrder()
            }
        }
    }
    
    func createNewOrder() {
        let parameters = ["key": API_KEY] as AnyObject
        let methodCall = "\(URLServer)newOrder"
        makeRequest(methodCall, metodo: .post, params: parameters) {
            response, error in
            if let ans = response {
                if ans["success"] as! Bool == true {
                    let answer = ans["order"] as! Int
                    if answer > 0 {
                        self.processOrder(answer, items: self.arrayIds as NSArray, quantity: self.arrayQty as NSArray)
                        self.productList.removeAll()
                        deleteDBContent(context: getGlobalManagedContext())
                        self.do_cell_refresh()
                    }
                    SwiftSpinner.hide()
                } else {
                    self.showMessage(ans["message"] as! String)
                    SwiftSpinner.hide()
                }
            } else {
                self.showMessage(NETWORK_ERROR)
                SwiftSpinner.hide()
            }
        }
    }
    
    
    //MARK : Table Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchController.isActive && searchController.searchBar.text != "" {
            return filteredItems.count
        }
        return productList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MMakeOrderCell") as! MMakeOrderTableViewCell

        let candy: Product
        let candies: [Product]
        if searchController.isActive && searchController.searchBar.text != "" {
            candy = filteredItems[indexPath.row]
            candies = filteredItems
        } else {
            candy = productList[indexPath.row]
            candies = productList
        }
        cell.qtySelected.addTarget(self, action: #selector(self.updateQtySelected(_:)), for: .editingDidEnd)
        cell.itemName.text = candy.name
        cell.itemQty.text = "\(candy.quantity!)"
        cell.itemQty.tag  = candies[indexPath.row].id
        cell.itemSelected.tag = candies[indexPath.row].id
        if let cellText = itemsToSend[candies[indexPath.row].id] {
            cell.qtySelected.text = cellText
        } else {
            cell.qtySelected.text = ""
        }
        if let cellStatus = switchToSend[candies[indexPath.row].id] {
            cell.itemSelected.isOn = cellStatus
        } else {
            cell.itemSelected.isOn = false
            if productsSelected.count > 0 {
                for psd in productsSelected {
                    if psd.id! == candy.id! {
                        cell.itemSelected.isOn = true
                        cell.qtySelected.text = "\(psd.requested!)"
                        self.changeItemStatus(cell.itemSelected)
                        self.updateQtySelected(cell.qtySelected)
                    }
                }
            }
        }
        cell.qtySelected.tag = candies[indexPath.row].id
        cell.qtySelected.keyboardType = .numberPad
        cell.itemSelected.addTarget(self, action: #selector(self.changeItemStatus), for: .valueChanged)
        
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        var numSections = 0
        if (self.productList.count > 0) {
            numSections = 1
        } else {
            let noDataLabel: UILabel = UILabel.init(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
            noDataLabel.text = "You have an open order"
            noDataLabel.numberOfLines = 6
            noDataLabel.textColor = UIColor.red
            noDataLabel.textAlignment = .center
            tableView.backgroundView = noDataLabel
            tableView.separatorStyle = .none
        }
        return numSections
    }
    
    //MARK: Bottom Tabs actions
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        switch item.tag {
        case 1:
            let main = self.storyboard?.instantiateViewController(withIdentifier: "MMain") as! MMainViewController
            self.navigationController?.pushViewController(main, animated: true)
        case 2:
            let driver = self.storyboard?.instantiateViewController(withIdentifier: "MDriver") as! MDriverViewController
            self.navigationController?.pushViewController(driver, animated: true)
        case 3:
            let inventory = self.storyboard?.instantiateViewController(withIdentifier: "MInventory") as! MInventoryViewController
            self.navigationController?.pushViewController(inventory, animated: true)
        default:
            return
        }
    }
    
    //MARK: Network request
    //func processOrder(item: Product, order: Int, content: AnyObject) {
    func processOrder(_ order: Int, items: NSArray, quantity: NSArray) {
        let parameters = ["key": API_KEY, "user": userGlobal, "itemId": items, "qty": quantity, "order": order] as AnyObject
        let methodCall = "\(URLServer)processOrder"
        makeRequest(methodCall, metodo: .post, params: parameters) {
            response, error in
            if let ans = response {
                if ans["success"] as! Bool == true {
                } else {
                    self.showMessage(ans["message"] as! String)
                }
            } else {
                self.showMessage(NETWORK_ERROR)
            }
        }
    }
    
    func getInventory() {
        let parameters = [] as AnyObject
        let methodCall = "\(URLServer)getInventory"
        makeRequest(methodCall, metodo: .get, params: parameters) {
            response, error in
            if let ans = response {
                let content = ans["content"] as! [[String: AnyObject]]
                self.btnSubmit.isHidden = false
                for prd in content {
                    let itemId    = Int(prd["items_id"] as! String)
                    let itemName  = prd["description"] as! String
                    let itemPrice = Double(prd["price"] as! String)
                    var itemQty = Double(0)
                    if let validQty = Double(prd["quantity"] as! String) {
                        itemQty = validQty
                    }
                    let itemSkids = Int(prd["skids"] as! String)
                    let totalBoxes = Int(prd["total_boxes"] as! String)
                    let internList : Product = Product(id: itemId!, name: itemName, price: itemPrice!, quantity: itemQty, skids: itemSkids!, totalBoxes: totalBoxes!)
                    self.productList.append(internList)
                }
                
                //TODO: get items from database
                self.productsSelected = getAllProductsOnCart(context: getGlobalManagedContext())
                self.do_cell_refresh()
            }
        }
    }
    
    func checkOpenOrder() {
        let parameters = ["key":API_KEY] as AnyObject
        let methodCall = "\(URLServer)checkOrderExists"
        makeRequest(methodCall, metodo: .get, params: parameters) {
            response, error in
            if let ans = response {
                if ans["success"] as! Bool == true {
                    self.getInventory()
                } else {
                    self.btnSubmit.isHidden = true
                    //self.showMessage(ans["message"] as! String)
                    self.do_cell_refresh()
                }
            } else {
                self.showMessage(NETWORK_ERROR)
            }
        }
    }
    
    func do_cell_refresh() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView?.reloadData()
        return
    }
    
    func showMessage(_ message: String) {
        let alert = UIAlertController(title: APP_NAME, message: message, preferredStyle: .alert)
        let cancel = UIAlertAction(title: "Close", style: .cancel, handler: { (action) -> Void in })
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func filterContentForSearchText(_ searchText: String, scope: String = "All") {
        filteredItems = productList.filter { product in
            return product.name.lowercased().contains(searchText.lowercased())
        }
        
        tableView.reloadData()
    }
    
    func checkIfMarket() {
        var role = 0
        if let dictionary = Locksmith.loadDataForUserAccount(userAccount: "user") {
            role = dictionary["role"] as! Int
        }
        
        if role != 3 {
            deleteLogOut()
            let out = self.storyboard?.instantiateViewController(withIdentifier: "Home") as! HomeViewController
            self.navigationController?.pushViewController(out, animated: true)
        }
    }
    
    @IBAction func showScanner(_ sender: Any) {
        let scanner = storyboard?.instantiateViewController(withIdentifier: "Scanner") as! ScannerViewController
        scanner.kindScreen = 2
        navigationController?.pushViewController(scanner, animated: true)
    }
    


}


extension MMakeOrderViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        filterContentForSearchText(searchController.searchBar.text!)
    }
}


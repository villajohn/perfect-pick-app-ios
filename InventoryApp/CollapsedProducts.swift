//
//  CollapsedProducts.swift
//  PerfectPick
//
//  Created by Jhon Villalobos on 8/23/16.
//  Copyright © 2016 Jhon Villalobos. All rights reserved.
//

import Foundation

class CollapsedProducts {
    var categoryName: String!
    var items: [Product]!
    var collapsed: Bool!
    
    init(categoryName: String, items: [Product], collapsed: Bool = false) {
        self.categoryName = categoryName
        self.items = items
        self.collapsed = collapsed
    }
    
}
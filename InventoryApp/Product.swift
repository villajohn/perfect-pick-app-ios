//
//  Product.swift
//  InventoryApp
//
//  Created by Jhon Villalobos on 7/1/16.
//  Copyright © 2016 Jhon Villalobos. All rights reserved.
//

import Foundation

class Product {
    var id:Int!
    var name:String!
    var price:Double!
    var quantity:Double!
    var skids: Int!
    var totalBoxes: Int!
    
    init(id: Int, name: String, price: Double, quantity: Double, skids: Int, totalBoxes: Int) {
        self.name     = name
        self.price    = price
        self.quantity = quantity
        self.id       = id
        self.skids    = skids   
        self.totalBoxes = totalBoxes
    }
    
    init(prdDictionary: [String: AnyObject]) {
        self.id                         = Int((prdDictionary["items_id"] as? String)!)
        self.name                       = prdDictionary["description"] as? String
        self.totalBoxes                 = Int((prdDictionary["total_boxes"] as? String)!)
        self.quantity                   = Double((prdDictionary["quantity"] as! String))
        self.price                      = Double((prdDictionary["price"] as! String))
        self.skids                      = Int((prdDictionary["skids"] as? String)!)
    }
    
    static func populateProduct(data: NSDictionary) -> Product {
        var product = Product(id: 0, name: "", price: Double(0), quantity: Double(0), skids: 0, totalBoxes: 0)
        product = Product(prdDictionary: data["content"] as! [String: AnyObject])
        return product
    }
}

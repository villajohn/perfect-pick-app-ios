//
//  WMarketViewController.swift
//  PerfectPick
//
//  Created by Jhon Villalobos on 7/7/16.
//  Copyright © 2016 Jhon Villalobos. All rights reserved.
//

import UIKit
import SwiftSpinner
import Locksmith

class WMarketViewController: UIViewController, UITabBarDelegate, UITableViewDataSource, UITableViewDelegate {
    
    var orderList = [Order]()
    
    @IBOutlet weak var titlesContent: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tabBar: UITabBar!
    
    var originRequest : Int?

    override func viewDidLoad() {
        super.viewDidLoad()
        checkIfWarehouse()

        self.title = "Market Order"
        self.titlesContent.backgroundColor = UIColor(colorLiteralRed: 60/255.0, green: 60/255.0, blue: 60/255.0, alpha: 1.0)
        self.titlesContent.layer.cornerRadius = 5.0
        
        self.tabBar.delegate = self
        self.tabBar.barTintColor = UIColor.black
        
        SwiftSpinner.show("loading...")
        self.getOrder()
        SwiftSpinner.hide()
        
    }
    
    //MARK : Table Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orderList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WMarketCell") as! WMarketTableViewCell
        
        let items = orderList[indexPath.row]
        cell.itemName.text = items.itemName
        cell.itemBoxes.text = "\(items.qty!)"
        
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        var numSections = 0
        if (self.orderList.count > 0) {
            numSections = 1
        } else {
            let noDataLabel: UILabel = UILabel.init(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
            noDataLabel.text = "There no order available yet. \n Reasons Why? \n 1. Market never made an order. \n 2. Market has yet to send order request to you."
            noDataLabel.numberOfLines = 6
            noDataLabel.textColor = UIColor.red
            noDataLabel.textAlignment = .center
            tableView.backgroundView = noDataLabel
            tableView.separatorStyle = .none
        }
        return numSections
    }
    
    //MARK: Bottom Tabs actions
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        switch item.tag {
        case 1:
            if self.originRequest == 2 {
                let main = self.storyboard?.instantiateViewController(withIdentifier: "MInventory") as! MInventoryViewController
                main.originRequest = 2
                self.navigationController?.pushViewController(main, animated: true)
            } else {
                let main = self.storyboard?.instantiateViewController(withIdentifier: "WMain") as! WMainViewController
                self.navigationController?.pushViewController(main, animated: true)
            }
        case 2:
            if self.originRequest == 2 {
                let driver = self.storyboard?.instantiateViewController(withIdentifier: "DMain") as! DMainViewController
                self.navigationController?.pushViewController(driver, animated: true)
            } else {
                let driver = self.storyboard?.instantiateViewController(withIdentifier: "WDriver") as! WDriverViewController
                driver.originRequest = 1
                self.navigationController?.pushViewController(driver, animated: true)
            }
        default:
            return
        }
    }
    
    // MARK: Network methods
    func getOrder() {
        let parameters = ["key": API_KEY, "status": 1] as AnyObject
        let methodCall = "\(URLServer)getMarketOrder"
        makeRequest(methodCall, metodo: .get, params: parameters) {
            response, error in
            if let ans = response {
                if ans["success"] as! Bool == true {
                    let content = ans["content"] as! [[String:AnyObject]]
                    let itemsOrdered = content.count
                    self.orderList.removeAll()
                    if itemsOrdered > 0 {
                        for prd in content {
                            
                            let idRow = Int(prd["id"] as! String)!
                            let qtyItem  = Int(prd["quantity"] as! String)!
                            let qtyConfirmed = Int(prd["quantity"] as! String)!
                            let dateItem = convertStringDate(prd["date"] as! String)
                            let locat    = Int(prd["locations_id"] as! String)!
                            let itId     = Int(prd["items_id"] as! String)!
                            let userId   = Int(prd["users_id"] as! String)!
                            let itemName = prd["name"] as! String
                            let itemStatus = Int(prd["confirmed"] as! String)
                            let orderId  = Int(prd["orders_id"] as! String)
                            let locationName = prd["location"] as! String
                            let switchStatus = (itemStatus == 1 ? true : false)
                            
                            let fillingList : Order = Order(id: idRow, qty: qtyItem, qtyConfirmed: qtyConfirmed, date: dateItem, locId: locat, itemId: itId, marketId: userId, itemName: itemName, status: switchStatus, orderId: orderId, locationName: locationName)
                            self.orderList.append(fillingList)
                        }
                
                    } else {
                        self.showMessage("No pending orders")
                    }
                    self.do_cell_refresh()
                } else {
                    self.showMessage(ans["message"] as! String)
                }
            } else {
                self.showMessage(NETWORK_ERROR)
            }
        }
    }
    
    func do_cell_refresh() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView?.reloadData()
        return
    }
    
    //MARK: Alerts
    func showMessage(_ message: String) {
        let alert = UIAlertController(title: APP_NAME, message: message, preferredStyle: .alert)
        let cancel = UIAlertAction(title: "Close", style: .cancel, handler: { (action) -> Void in })
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func checkIfWarehouse() {
        var role = 0
        if let dictionary = Locksmith.loadDataForUserAccount(userAccount: "user") {
            role = dictionary["role"] as! Int
        }
        
        if role != 1 {
            deleteLogOut()
            let out = self.storyboard?.instantiateViewController(withIdentifier: "Home") as! HomeViewController
            self.navigationController?.pushViewController(out, animated: true)
        }
    }

    

}

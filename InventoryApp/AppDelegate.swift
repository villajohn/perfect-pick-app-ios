//
//  AppDelegate.swift
//  InventoryApp
//
//  Created by Jhon Villalobos on 6/30/16.
//  Copyright © 2016 Jhon Villalobos. All rights reserved.
//

import UIKit
import Locksmith
import CoreData
import Fabric
import Crashlytics


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        //Register for push notifications
        let settings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
        UIApplication.shared.registerUserNotificationSettings(settings)
        UIApplication.shared.registerForRemoteNotifications()
        
        //delete all notification records from core data
        cleanNotification()
        
        // 1 Check if launched from notification
        if let notification = launchOptions?[UIApplicationLaunchOptionsKey.remoteNotification] as? [String: AnyObject] {
            // 2
            let aps = notification["aps"] as! [String: AnyObject]

            let tail = aps["role"] as! Int
            
            // 3
            let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
            switch tail {
            case 1:
                do {
                    try Locksmith.updateData(data: ["role": 3], forUserAccount: "notification")
                } catch {}
                let baVC = mainStoryboard.instantiateViewController(withIdentifier: "OrderRequest") as! OrderRequestViewController
                let talco : UIViewController = (self.window?.rootViewController)!
                talco.present(baVC, animated: true, completion: nil)
            case 2:
                let baVC = mainStoryboard.instantiateViewController(withIdentifier: "DMain") as! DMainViewController
                let talco : UIViewController = (self.window?.rootViewController)!
                talco.present(baVC, animated: true, completion: nil)
            case 3:
                let baVC = mainStoryboard.instantiateViewController(withIdentifier: "MMain") as! MMainViewController
                let talco : UIViewController = (self.window?.rootViewController)!
                talco.present(baVC, animated: true, completion: nil)
            default:
                let baVC = mainStoryboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
                let talco : UIViewController = (self.window?.rootViewController)!
                talco.present(baVC, animated: true, completion: nil)
            }
        }
        Fabric.with([Crashlytics.self])

        return true
    }
    
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let token = convertDeviceTokenToString(deviceToken)
        
        //send to server
        var userId = 0
        if let dictionary = Locksmith.loadDataForUserAccount(userAccount: "user") {
            userId   = dictionary["id"] as! Int
        }
        
        let parameters = ["device": token, "userId": userId, "app": "iphone"] as AnyObject
        let methodUrl  = "\(URLServer)saveDevice"
        makeRequest(methodUrl, metodo: .post, params: parameters) {
            response, error in
        }
        UIApplication.shared.cancelAllLocalNotifications()
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Device token for push notifications: FAIL -- ")
        print(error)
    }
    
    fileprivate func convertDeviceTokenToString(_ deviceToken:Data) -> String {
        //  Convert binary Device Token to a String (and remove the <,> and white space charaters).
        var deviceTokenStr = deviceToken.description.replacingOccurrences(of: ">", with: "", options: [], range: nil)
        deviceTokenStr = deviceTokenStr.replacingOccurrences(of: "<", with: "", options: [], range: nil)
        deviceTokenStr = deviceTokenStr.replacingOccurrences(of: " ", with: "", options: [], range: nil)
        
        // Our API returns token in all uppercase, regardless how it was originally sent.
        // To make the two consistent, I am uppercasing the token string here.
        deviceTokenStr = deviceTokenStr.uppercased()
        return deviceTokenStr
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        self.saveContext()
    }
    
    func application(_ application: UIApplication, didReceive notification: UILocalNotification) {
        print("local")
        print(notification)
        if let userInfo = notification.userInfo {
            if userInfo.count > 0 {
                print(userInfo)
            }
        }
        
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        let aps = userInfo["aps"] as! [String: AnyObject]
        
        let tail = aps["role"] as! Int
        
        // 3
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        switch tail {
        case 1:
            do {
                try Locksmith.updateData(data: ["role": 3], forUserAccount: "notification")
            } catch {}
            completionHandler(.newData)
            
            let VC1 = mainStoryboard.instantiateViewController(withIdentifier: "OrderRequest") as! OrderRequestViewController
            let navController = UINavigationController(rootViewController: VC1) // Creating a navigation controller with VC1 at the root of the navigation stack.
            self.window?.rootViewController!.present(navController, animated:true, completion: nil)
        case 2:
            let VC1 = mainStoryboard.instantiateViewController(withIdentifier: "DMain") as! DMainViewController
            let navController = UINavigationController(rootViewController: VC1) // Creating a navigation controller with VC1 at the root of the navigation stack.
            self.window?.rootViewController!.present(navController, animated:true, completion: nil)
            completionHandler(.newData)
        case 3:
            let VC1 = mainStoryboard.instantiateViewController(withIdentifier: "MMain") as! MMainViewController
            let navController = UINavigationController(rootViewController: VC1) // Creating a navigation controller with VC1 at the root of the navigation stack.
            self.window?.rootViewController!.present(navController, animated:true, completion: nil)
            completionHandler(.newData)
        default:
            let baVC = mainStoryboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
            let talco : UIViewController = (self.window?.rootViewController)!
            talco.present(baVC, animated: true, completion: nil)
            completionHandler(.newData)
        }
    }
    
    // MARK: - Core Data stack
    @available(iOS 10.0, *)
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "PerfectPickModel")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        if #available(iOS 10.0, *) {
            let context = persistentContainer.viewContext
            
            if context.hasChanges {
                do {
                    try context.save()
                } catch {
                    // Replace this implementation with code to handle the error appropriately.
                    // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    let nserror = error as NSError
                    fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
                }
            }
        }
    }


}


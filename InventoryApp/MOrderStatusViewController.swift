//
//  MOrderStatusViewController.swift
//  PerfectPick
//
//  Created by Jhon Villalobos on 7/8/16.
//  Copyright © 2016 Jhon Villalobos. All rights reserved.
//

import UIKit
import SwiftSpinner
import Locksmith

class MOrderStatusViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UITabBarDelegate {
    
    @IBOutlet weak var tabBar: UITabBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var titleContainer: UIView!
    @IBOutlet weak var btnAccept: UIButton!
    
    var refreshControl : UIRefreshControl!
    var orderList = [Order]()
    
    var originRequest : Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        checkIfMarket()
        self.title = "Order Received"

        
        self.tabBar.delegate = self
        self.tabBar.barTintColor = UIColor.black
        
        self.titleContainer.backgroundColor = UIColor(colorLiteralRed: 60/255.0, green: 60/255.0, blue: 60/255.0, alpha: 1.0)
        self.titleContainer.layer.cornerRadius = 5.0
        
        self.btnAccept.layer.cornerRadius = 5.0
        
        loadRefreshControl()
        getOrder()
    }
    
    
    //MARK : Table Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orderList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MOrderStatusCell") as! MOrderStatusTableViewCell
        let items = orderList[indexPath.row]
        cell.itemName.text = items.itemName
        cell.itemBoxes.text = "\(items.qtyConfirmed!)"
        
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        var numSections = 0
        if (self.orderList.count > 0) {
            numSections = 1
        } else {
            let noDataLabel: UILabel = UILabel.init(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
            noDataLabel.text = "There no order available yet. \n Reasons Why? \n 1. You don't have an open order. \n 2. Driver has not yet loaded the order on the truck."
            noDataLabel.numberOfLines = 6
            noDataLabel.textColor = UIColor.red
            noDataLabel.textAlignment = .center
            tableView.backgroundView = noDataLabel
            tableView.separatorStyle = .none
        }
        return numSections
    }
    
    //MARK: Bottom Tabs actions
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        switch item.tag {
        case 1:
            let main = self.storyboard?.instantiateViewController(withIdentifier: "MMain") as! MMainViewController
            self.navigationController?.pushViewController(main, animated: true)
        case 2:
            let market = self.storyboard?.instantiateViewController(withIdentifier: "MDriver") as! MDriverViewController
            self.navigationController?.pushViewController(market, animated: true)
        default:
            return
        }
    }
    
    // MARK: Network methods
    func getOrder() {
        let statusOrder = Status.StatusLibrary().library["Transit"]
        let parameters = ["key": API_KEY, "status": statusOrder!] as AnyObject
        let methodCall = "\(URLServer)getMarketOrder"
        makeRequest(methodCall, metodo: .get, params: parameters) {
            response, error in
            if let ans = response {
                if ans["success"] as! Bool == true {
                    let content = ans["content"] as! [[String:AnyObject]]
                    self.orderList.removeAll()
                    if content.count > 0 {
                        self.btnAccept.isEnabled = true
                        for prd in content {
                            let idRow    = Int(prd["id"] as! String)!
                            let qtyItem  = Int(prd["quantity"] as! String)!
                            let qtyConfirmed = Int(prd["qtysold"] as! String)!
                            let dateItem = convertStringDate(prd["date"] as! String)
                            let locat    = Int(prd["locations_id"] as! String)!
                            let itId     = Int(prd["items_id"] as! String)!
                            let userId   = Int(prd["users_id"] as! String)!
                            let itemName = prd["name"] as! String
                            let itemStatus = Int(prd["confirmed"] as! String)
                            let orderId  = Int(prd["orders_id"] as! String)
                            let locationName = prd["location"] as! String
                            let switchStatus = (itemStatus == 1 ? true : false)
                            
                            let fillingList : Order = Order(id: idRow, qty: qtyItem, qtyConfirmed: qtyConfirmed, date: dateItem, locId: locat, itemId: itId, marketId: userId, itemName: itemName, status: switchStatus, orderId: orderId, locationName: locationName)
                            self.orderList.append(fillingList)
                        }
                        self.do_cell_refresh()
                    } else {
                        self.orderList.removeAll()
                        self.do_cell_refresh()
                        self.btnAccept.isEnabled = false
                        //self.showMessage("No pending orders")
                    }
                } else {
                    self.orderList.removeAll()
                    self.do_cell_refresh()
                    self.btnAccept.isEnabled = false
                    //self.showMessage(ans["message"] as! String)
                }
            } else {
                self.showMessage(NETWORK_ERROR)
            }
        }
    }
    
    func do_cell_refresh() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView?.reloadData()
        return
    }
    
    func updateTable() {
        self.getOrder()
    }
    
    func loadRefreshControl() {
        refreshControl = UIRefreshControl()
        refreshControl.backgroundColor = UIColor.red
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to update data")
        refreshControl.addTarget(self, action: #selector(MOrderStatusViewController.refreshTable), for: .valueChanged)
        self.tableView.addSubview(refreshControl)
    }
    
    func refreshTable() {
        updateTable()
        refreshControl.endRefreshing()
    }
    
    // ******
    
    func showMessage(_ message: String) {
        let alert = UIAlertController(title: APP_NAME, message: message, preferredStyle: .alert)
        let cancel = UIAlertAction(title: "Close", style: .cancel, handler: { (action) -> Void in })
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnAcceptOrder(_ sender: AnyObject) {
        SwiftSpinner.show("sending order...")
        processOrderPrompt()
        SwiftSpinner.hide()
    }
    
    func processOrderPrompt() {
        let alert = UIAlertController(title: APP_NAME, message: "Are you sure this order has completed?", preferredStyle: .alert)
        let cancel = UIAlertAction(title: "No", style: .cancel, handler: { (action) -> Void in })
        let doIt = UIAlertAction(title: "Yes", style: .default, handler: {
            (action) -> Void in
            
            let orderId = self.orderList[0].orderId
            
            //Change status order
            let parameters = ["key": API_KEY, "order": orderId!, "user": userGlobal] as AnyObject
            let methodCall = "\(URLServer)confirmOrderReceived"
            makeRequest(methodCall, metodo: .post, params: parameters) {
                response, error in
                if let ans = response {
                    if ans["success"] as! Bool == true {
                        self.orderSentMessage(ans["message"] as! String)
                    } else {
                        self.showMessage(ans["message"] as! String)
                    }
                } else {
                    self.showMessage(NETWORK_ERROR)
                }
            }
            
        })
        alert.addAction(cancel)
        alert.addAction(doIt)
        self.present(alert, animated: true, completion: nil)
    }
    
    func orderSentMessage(_ message: String) {
        let alert = UIAlertController(title: APP_NAME, message: message, preferredStyle: .alert)
        let close = UIAlertAction(title: "OK", style: .default, handler: {
            (action) -> Void in
            SwiftSpinner.show("loading...")
            self.getOrder()
            SwiftSpinner.hide()
        })
        alert.addAction(close)
        self.present(alert, animated: true, completion: nil)
    }
    
    func checkIfMarket() {
        var role = 0
        if let dictionary = Locksmith.loadDataForUserAccount(userAccount: "user") {
            role = dictionary["role"] as! Int
        }
        
        if role != 3 {
            deleteLogOut()
            let out = self.storyboard?.instantiateViewController(withIdentifier: "Home") as! HomeViewController
            self.navigationController?.pushViewController(out, animated: true)
        }
    }

}

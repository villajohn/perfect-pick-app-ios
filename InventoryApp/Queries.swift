//
//  Queries.swift
//  PerfectPick
//
//  Created by Jhon Villalobos on 2/1/17.
//  Copyright © 2017 Jhon Villalobos. All rights reserved.
//

import Foundation
import CoreData
import UIKit

func getGlobalManagedContext() -> NSManagedObjectContext {
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    var localManagedContext : NSManagedObjectContext
    
    if #available(iOS 10.0, *) {
        localManagedContext = appDelegate.persistentContainer.viewContext
    } else {
        // iOS 9.0 and below - however you were previously handling it
        guard let modelURL = Bundle.main.url(forResource: "PerfectPickModel", withExtension:"momd") else {
            fatalError("Error loading model from bundle")
        }
        guard let mom = NSManagedObjectModel(contentsOf: modelURL) else {
            fatalError("Error initializing mom from: \(modelURL)")
        }
        let psc = NSPersistentStoreCoordinator(managedObjectModel: mom)
        localManagedContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let docURL = urls[urls.endIndex-1]
        let storeURL = docURL.appendingPathComponent("PerfectPickModel")
        do {
            try psc.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: storeURL, options: nil)
        } catch {
            fatalError("Error migrating store: \(error)")
        }
    }
    
    return localManagedContext
}

func saveModel(id: Int, qty: Int) -> Bool{
    
    let managedContext = getGlobalManagedContext()
    
    //2
    let entity = NSEntityDescription.entity(forEntityName: "Requested", in: managedContext)
    let orderItem = NSManagedObject(entity: entity!, insertInto: managedContext)
    
    //3
    //save the main item
    orderItem.setValue(id, forKey: "id")
    orderItem.setValue(qty, forKey: "requested")
    
    //4 Save main product
    do {
        try managedContext.save()
        return true
    } catch let error as NSError {
        print(error)
        return false
    }
    
    //*** end saving special items
    
    
    //let fetchRequest : NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: "TBL_ORDERITEMS")
    //let fetchRequest2 : NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: "TBL_ORDERTOPPING")
    
    //DELETE TABLE
    /*
     do {
     let results = try managedContext.fetch(fetchRequest)
     let orderItem : [NSManagedObject] = results as! [NSManagedObject]
     print(orderItem.count)
     for ord in results {
     print((ord as AnyObject).value(forKey: "ori_id"))
     print((ord as AnyObject).value(forKey: "ori_quantity"))
     print((ord as AnyObject).value(forKey: "ori_price"))
     
     managedContext.delete(ord as! NSManagedObject)
     }
     
     let results2 = try managedContext.fetch(fetchRequest2)
     let orderItem2 : [NSManagedObject] = results as! [NSManagedObject]
     print(orderItem2.count)
     for ord in results2 {
     print((ord as AnyObject).value(forKey: "oro_id"))
     print((ord as AnyObject).value(forKey: "oro_name"))
     print((ord as AnyObject).value(forKey: "oro_price"))
     print((ord as AnyObject).value(forKey: "oro_optype"))
     
     managedContext.delete(ord as! NSManagedObject)
     }
     
     try managedContext.save()
     
     } catch {}
     */
}

func getAllProductsOnCart(context: NSManagedObjectContext) -> [ProductOrder] {
    
    var itemsCart = [ProductOrder]()
    
    let fetchRequest : NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: "Requested")
    
    do {
        let results = try context.fetch(fetchRequest)
        let orderItem : [NSManagedObject] = results as! [NSManagedObject]
        
        for prd in orderItem {
            let objectId = (prd as AnyObject).value(forKey: "id") as! Int
            let objectQty   = (prd as AnyObject).value(forKey: "requested") as! Int
            
            let newProduct = ProductOrder(selected: true, id: objectId, name: "", price: Double(0), quantity: Double(0), skids: 0, totalBoxes: 0, requested: objectQty)
            
            itemsCart.append(newProduct)
        }
        
    } catch {}
    
    return itemsCart
}

func deleteDBContent(context: NSManagedObjectContext) {
    
    let fetchRequest : NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: "Requested")
    
    
    do {
        let results = try context.fetch(fetchRequest)
        for ord in results {
            context.delete(ord as! NSManagedObject)
        }
        
        try context.save()
        
    } catch {}
}

//
//  WMainViewController.swift
//  InventoryApp
//
//  Created by Jhon Villalobos on 6/30/16.
//  Copyright © 2016 Jhon Villalobos. All rights reserved.
//

import UIKit
import SwiftSpinner
import Locksmith

class WMainViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UITabBarDelegate, UIPickerViewDelegate {
    
    var listItems   = [CollapsedProducts]()
    var productList = [Product]()
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var titlesContainer: UIView!
    @IBOutlet weak var btnRequest: UIButton!
    @IBOutlet weak var tabItemWarehouse: UITabBarItem!
    @IBOutlet weak var tabBar: UITabBar!
    @IBOutlet weak var titleQuantity: UIButton!
    
    let searchController = UISearchController(searchResultsController: nil)
    var filteredItems = [Product]()
    var itemsForFilter = [Product]()
    
    var isSkid = true
    
    var refreshControl: UIRefreshControl!
    
    var categoryToSend = 0
    
    //picker
    let pickerCategory     = UIPickerView()
    var categoryDetails    = [SingleItem]()
    var categorySelected   : SingleItem = SingleItem(id: 0, name: "")

    override func viewDidLoad() {
        super.viewDidLoad()
        checkIfWarehouse()
        //checkIfNotification()
        
        self.title = "Warehouse"
        
        self.tabBar.barTintColor = UIColor.black
        
        btnRequest.layer.cornerRadius = 5.0
        
        self.tabBar.delegate = self
        
        self.navigationItem.backBarButtonItem?.tintColor = UIColor.white
        self.titlesContainer.tintColor = UIColor.white
        self.titlesContainer.backgroundColor = UIColor(colorLiteralRed: 60/255.0, green: 60/255.0, blue: 60/255.0, alpha: 1.0)
        self.titlesContainer.layer.cornerRadius = 5.0
        
        //Search functions
        
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        definesPresentationContext = true
        tableView.tableHeaderView = searchController.searchBar
        
        loadRefreshControl()
        getInventory()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    func getInventory() {
        SwiftSpinner.show("loading...")
        let parameters = [] as AnyObject
        let methodCall = "\(URLServer)getInventoryAlphabetically"
        makeRequest(methodCall, metodo: .get, params: parameters) {
            response, error in
            if let ans = response {
                let content = ans["content"] as! [[String:AnyObject]]
        
                self.listItems.removeAll()
                self.itemsForFilter.removeAll()
                for prd in content {
                    self.productList.removeAll()
                    let newItem : CollapsedProducts = CollapsedProducts(categoryName: "", items: self.productList)
                    let categoryName = prd["name"] as! String
                    let catItem = SingleItem(id: Int(prd["id"] as! String)!, name: categoryName)
                    self.categoryDetails.append(catItem)
                    if let tal = prd["items"]   {
                        let items = tal as! [[String:AnyObject]]
                        for e in items {
                            let itemId    = Int(e["items_id"] as! String)
                            let itemName  = e["description"] as! String
                            let itemPrice = Double(e["price"] as! String)
                            var itemQty = Double(0)
                            if let validQty = Double(e["quantity"] as! String) {
                                itemQty = validQty
                            }
                            let itemSkids = Int(e["skids"] as! String)
                            let totalBoxes = Int(e["total_boxes"] as! String)
                            let internList : Product = Product(id: itemId!, name: itemName, price: itemPrice!, quantity: itemQty, skids: itemSkids!, totalBoxes: totalBoxes!)
                            self.productList.append(internList)
                            self.itemsForFilter.append(internList)
                        }
                    } else {
                        let internList : Product = Product(id: 0, name: "", price: 0, quantity: 0, skids: 0, totalBoxes: 0)
                        self.productList.append(internList)
                    }
                    newItem.categoryName = categoryName
                    newItem.items =  self.productList
                    self.listItems.append(newItem)
                }
                self.do_cell_refresh()
            } else {
                SwiftSpinner.hide()
                globalMessage(APP_NAME as NSString, msgBody: NETWORK_ERROR as NSString, delegate: nil, self: self)
            }
        }
    }

    // MARK : Table Delegates
    func numberOfSections(in tableView: UITableView) -> Int {
        if searchController.isActive && searchController.searchBar.text != "" {
            return 1
        }
        return listItems.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchController.isActive && searchController.searchBar.text != "" {
            return filteredItems.count
        }
        return (listItems[section].collapsed!) ? 0 : listItems[section].items.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableCell(withIdentifier: "WMainHeader") as! WMainHeaderCell
        
        if searchController.isActive && searchController.searchBar.text != "" {
            header.titleButton.setTitle("Filtered List", for: UIControlState())
            return header.contentView
        }
        
        header.toggleButton.tag = section
        header.titleButton.tag = section
        header.titleButton.setTitle(listItems[section].categoryName, for: UIControlState())
        header.toggleButton.rotate(listItems[section].collapsed! ? 0.0 : CGFloat(Double.pi / 2))
        header.titleButton.addTarget(self, action: #selector(self.toggleCollapse(_:)), for: .touchUpInside)
        header.toggleButton.addTarget(self, action: #selector(self.toggleCollapse), for: .touchUpInside)
        
        return header.contentView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "inventorycell", for: indexPath) as! WMainTableViewCell
        
        if listItems.count > 0 {
            let candy: Product
            if searchController.isActive && searchController.searchBar.text != "" {
                candy = filteredItems[indexPath.row]
            } else {
                candy = listItems[indexPath.section].items[indexPath.row]
            }
            cell.productName.text      = candy.name
            //cheack what kind of quantity will show
            switch isSkid {
            case false:
                self.titleQuantity.setTitle("# of Boxes", for: UIControlState())
                cell.productQuantity.text  = "\(candy.totalBoxes!)"
            case true:
                self.titleQuantity.setTitle("# of Skids", for: UIControlState())
                cell.productQuantity.text  = "\(candy.quantity!)"
            }
        }
        return cell
    }
    
    
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let delete = UITableViewRowAction(style: .destructive, title: "Delete") { (action, indexPath) in
            let product = self.listItems[indexPath.section].items[indexPath.row]
            self.deleteRequest(product.id)
        }
        
        return [delete]
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var itemSelected : Product
        if searchController.isActive && searchController.searchBar.text != "" {
            itemSelected = filteredItems[indexPath.row]
        } else {
            itemSelected = listItems[indexPath.section].items[indexPath.row]
        }
        let itemName     = itemSelected.name
        let itemQty      = itemSelected.quantity
        let itemId       = itemSelected.id
        let itemPrice    = itemSelected.price
        let boxes        = itemSelected.totalBoxes
        self.showEditWindow(itemName!, qty: Int(itemQty!), itemId: itemId!, itemPrice: itemPrice!, boxes: boxes!)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60.0
    }
    
    //this is the function where the category header is collpased
    func toggleCollapse(_ sender: UIButton) {
        /*
        let section = sender.tag
        let collapsed = listItems[section].collapsed // sections[section].collapsed
        
        // Toggle collapse
        listItems[section].collapsed = !collapsed
        
        // Reload section
        tableView.reloadSections(NSIndexSet(index: section), withRowAnimation: .Automatic)
        */
    }
    
    func do_cell_refresh() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView?.reloadData()
        SwiftSpinner.hide()
    }
    // ******
    
    @IBAction func buttonRequestMarket(_ sender: AnyObject) {
        
    }
    
    
    //MARK : Alert View 
    var itemTxField: UITextField!
    var categoryTxField: UITextField!
    var boxesTxField: UITextField!
    var skidsTxField: UITextField!
    func showEditWindow(_ name: String, qty: Int, itemId: Int, itemPrice: Double, boxes: Int) {
        let alert = UIAlertController(title: APP_NAME, message: "Edit Item",
                                      preferredStyle: .alert)
        alert.addTextField(configurationHandler: configurationItemField)
        alert.addTextField(configurationHandler: configurationBoxField)
        itemTxField.text  = name
        boxesTxField.text = "\(boxes)"
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) -> Void in })
        alert.addAction(cancel)
        let doIt = UIAlertAction(title: "Ok", style: .default, handler: {
            (action) -> Void in
            SwiftSpinner.show("editing...")
            if Int(self.boxesTxField.text!) != nil {
                self.updateItem(self.itemTxField.text!, itemQty: Int(self.boxesTxField.text!)!, itemId: itemId, itemPrice: itemPrice, skids: Int(self.boxesTxField.text!)!, category: 0)
            } else {
                self.showMessage("The field for boxes must be a number")
            }
            SwiftSpinner.hide()
        })
        alert.addAction(doIt)
        self.present(alert, animated: true, completion: nil)
    }
    
    func showNewItemWindow() {
        let alert = UIAlertController(title: APP_NAME, message: "New Item", preferredStyle: .alert)
        alert.addTextField(configurationHandler: configurationItemField)
        alert.addTextField(configurationHandler: configurationCategoryField)
        alert.addTextField(configurationHandler: configurationBoxField)
        alert.addTextField(configurationHandler: configurationSkidsField)
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) -> Void in })
        alert.addAction(cancel)
        let create = UIAlertAction(title: "Create", style: .default, handler: {
            (action) -> Void in
            if self.itemTxField.text?.isEmpty == false && Int(self.boxesTxField.text!) != nil {
                SwiftSpinner.show("saving...")
                self.updateItem(self.itemTxField.text!, itemQty: Int(self.boxesTxField.text!)!, itemId: 0, itemPrice: 0.00, skids: Int(self.skidsTxField.text!)!, category: self.categoryToSend)
                SwiftSpinner.hide()
            } else {
                self.showMessage("Both fields are mandatory and Numb. of boxes must be a number")
            }
        })
        alert.addAction(create)
        self.present(alert, animated: true, completion: nil)
    }
    
    func showScanOption() {
        //let alert = UIAlertController(title: APP_NAME, message: "Do you want to use the scanner?", preferredStyle: .alert)
        let alert = UIAlertController()
        alert.title = APP_NAME
        alert.message = "What would you like to do?"
        let doIt = UIAlertAction(title: "Receive (Add to Inventory)", style: .default, handler: {
            (action) -> Void in
            //TODO SHOW SCANNER
            let scanner = self.storyboard?.instantiateViewController(withIdentifier: "Scanner") as! ScannerViewController
            scanner.kindScreen = 1
            scanner.isAdding = true
            self.navigationController?.pushViewController(scanner, animated: true)
            
        })
        let receive = UIAlertAction(title: "Ship (Subtract Inventory)", style: .default, handler: { (action) -> Void in
            let scanner = self.storyboard?.instantiateViewController(withIdentifier: "Scanner") as! ScannerViewController
            scanner.kindScreen = 1
            scanner.isAdding = false
            self.navigationController?.pushViewController(scanner, animated: true)
        })
        let close = UIAlertAction(title: "Close", style: .cancel, handler: { (action) -> Void in })
        alert.addAction(doIt)
        alert.addAction(receive)
        alert.addAction(close)
        self.present(alert, animated: true, completion: nil)
    }
    
    func configurationItemField(_ textField: UITextField!) {
        textField.placeholder = "Item Name"
        itemTxField = textField
    }
    
    func configurationCategoryField(_ textField: UITextField!) {
        textField.placeholder = "Select Category"
        pickerCategory.delegate = self
        pickerCategory.showsSelectionIndicator = true
        categoryTxField = textField
        pickerButtons(categoryTxField)
    }
    
    func configurationBoxField(_ textField: UITextField!) {
        textField.placeholder = "Numb. Boxes"
        textField.keyboardType = .numberPad
        boxesTxField = textField
    }
    
    func configurationSkidsField(_ textField: UITextField!) {
        textField.placeholder = "Boxes per Skid"
        textField.keyboardType = .numberPad
        skidsTxField = textField
    }
    
    
    func updateItem(_ itemName: String, itemQty: Int, itemId: Int, itemPrice: Double, skids: Int, category: Int) {
        let parameters = ["key": API_KEY, "id": itemId, "user": userGlobal, "description": itemName, "quantity": itemQty, "price": itemPrice, "skids": skids, "category": category] as AnyObject
        let methodCall = "\(URLServer)editItem"
        makeRequest(methodCall, metodo: .post, params: parameters) {
            response, error in
            if let ans = response {
                if ans["success"] as! Bool == true {
                    SwiftSpinner.show("saving...")
                    self.updateTable()
                    SwiftSpinner.hide()
                } else {
                    self.showMessage(ans["message"] as! String)
                }
            }
        }
    }
    
    func showMessage(_ message: String) {
        let alert = UIAlertController(title: APP_NAME, message: message, preferredStyle: .alert)
        let cancel = UIAlertAction(title: "Close", style: .cancel, handler: { (action) -> Void in })
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
    }
    
    func deleteRequest(_ id: Int) {
        let alert = UIAlertController()
        alert.title = APP_NAME
        alert.message = "Are you sure you want to delete this product?"
        let cancel = UIAlertAction(title: "No", style: .cancel, handler: { (action) -> Void in })
        let doIt = UIAlertAction(title: "Yes", style: .destructive, handler: { (action) -> Void in
            let parameters = ["key": API_KEY, "id": id, "user": userGlobal, ] as AnyObject
            let methodCall = "\(URLServer)deleteItem"
            makeRequest(methodCall, metodo: .post, params: parameters) {
                response, error in
                if let ans = response {
                    if ans["success"] as! Bool == true {
                        SwiftSpinner.show("updating...")
                        self.updateTable()
                        SwiftSpinner.hide()
                    } else {
                        self.showMessage(ans["message"] as! String)
                    }
                }
            }
        })
        alert.addAction(cancel)
        alert.addAction(doIt)
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func addItemButton(_ sender: AnyObject) {
        //showNewItemWindow() pepe
        showScanOption()
    }
    
    func updateTable() {
        self.getInventory()
    }
    
    func loadRefreshControl() {
        refreshControl = UIRefreshControl()
        refreshControl.backgroundColor = UIColor.red
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to update data")
        refreshControl.addTarget(self, action: #selector(WMainViewController.refreshTable), for: .valueChanged)
        self.tableView.addSubview(refreshControl)
    }
    
    func refreshTable() {
        updateTable()
        refreshControl.endRefreshing()
    }
    
    //MARK: Bottom Tabs actions 
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        switch item.tag {
        case 2:
            let driver = self.storyboard?.instantiateViewController(withIdentifier: "WDriver") as! WDriverViewController
            driver.originRequest = 1
            self.navigationController?.pushViewController(driver, animated: true)
        case 3:
            let market = self.storyboard?.instantiateViewController(withIdentifier: "WMarket") as! WMarketViewController
            self.navigationController?.pushViewController(market, animated: true)
        default:
            return
        }
    }
    
    @IBAction func btnLogOut(_ sender: AnyObject) {
        deleteLogOut()
        let out = self.storyboard?.instantiateViewController(withIdentifier: "Home") as! HomeViewController
        self.navigationController?.pushViewController(out, animated: true)
    }
    
    func filterContentForSearchText(_ searchText: String, scope: String = "All") {
        
        filteredItems = itemsForFilter.filter { product in
            return product.name.lowercased().contains(searchText.lowercased())
        }
        
        tableView.reloadData()
    }
    
    func checkIfWarehouse() {
//        var role = 0
//        if let dictionary = Locksmith.loadDataForUserAccount(userAccount: "user") {
//            role = dictionary["role"] as! Int
//        }
        
//        if role != 1 {
//            deleteLogOut()
//            let out = self.storyboard?.instantiateViewController(withIdentifier: "Home") as! HomeViewController
//            self.navigationController?.pushViewController(out, animated: true)
//        }
    }
    
    func checkIfNotification() {
        if let dictionary = Locksmith.loadDataForUserAccount(userAccount: "notification") {
            let role = dictionary["role"] as! Int
            if role == 3 {
                let out = self.storyboard?.instantiateViewController(withIdentifier: "OrderRequest") as! OrderRequestViewController
                self.navigationController?.pushViewController(out, animated: true)
            }
        }
    }
    
    @IBAction func changeQtyView(_ sender: AnyObject) {
        switch isSkid {
        case true:
            isSkid = false
        default:
            isSkid = true
        }
        self.do_cell_refresh()
    }
    
    //MARK: Pickers Button
    func pickerButtons(_ sender: UITextField) {
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 59/255, green: 89/255, blue: 152/255, alpha: 1)
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Ok", style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.donePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        
        toolBar.setItems([spaceButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        categoryTxField.inputView = pickerCategory
        
        sender.inputAccessoryView = toolBar
    }
    
    
    func donePicker(_ sender: UITextField) {
        self.categoryTxField.endEditing(true)
    }
    
    //MARK: Picker Delegate Methods
    func numberOfComponentsInPickerView(_ pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.categoryDetails[row].name
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.categoryDetails.count
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        categoryToSend = self.categoryDetails[row].id
        self.categoryTxField.text = self.categoryDetails[row].name
    }
    

}

extension WMainViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        filterContentForSearchText(searchController.searchBar.text!)
    }
}

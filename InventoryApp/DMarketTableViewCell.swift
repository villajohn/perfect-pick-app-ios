//
//  DMarketTableViewCell.swift
//  PerfectPick
//
//  Created by Jhon Villalobos on 7/9/16.
//  Copyright © 2016 Jhon Villalobos. All rights reserved.
//

import UIKit

class DMarketTableViewCell: UITableViewCell {
    
    @IBOutlet weak var itemName: UILabel!
    @IBOutlet weak var itemBoxes: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}

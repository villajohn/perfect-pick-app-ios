//
//  HomeViewController.swift
//  InventoryApp
//
//  Created by Jhon Villalobos on 6/30/16.
//  Copyright © 2016 Jhon Villalobos. All rights reserved.
//

import UIKit
import Locksmith

class HomeViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var userKeyField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        userKeyField.delegate = self
        self.title = APP_NAME
        
        hideKeyboardWhenTappedAround()
        
        //validateUserLoggedIn()
        checkIfNotification()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
        
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        userKeyField.resignFirstResponder()
        let parameters = ["username": textField.text!, "key": API_KEY] as AnyObject
        let methodCall = "\(URLServer)validateUser"
        makeRequest(methodCall, metodo: .post, params: parameters) {
            response, error in
            if let ans = response {
                if ans["success"] as! Bool == true {
                    let content = ans["content"] as! [String:AnyObject]
                    let role     = Int(content["role"] as! String)!
                    let usrName  = content["name"] as! String
                    let usrId    = Int(content["id"] as! String)
                    
                    userName     = usrName
                    userGlobal   = "\(usrId!)"
                    
                    self.saveUserLogedIn(role, userId: usrId!, userName: usrName)
                    self.processUserScreen(role)
                } else {
                    self.showMessage(ans["message"] as! String)
                }
            }
        }
        return true
    }
    
    func showMessage(_ message: String) {
        let alert = UIAlertController(title: APP_NAME, message: message, preferredStyle: .alert)
        let cancel = UIAlertAction(title: "Close", style: .cancel, handler: { (action) -> Void in })
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
    }
    
    //Show the screen for the user just loged in
    func processUserScreen(_ role: Int) {
        let backItem = UIBarButtonItem()
        backItem.tintColor = UIColor.white
        backItem.title = " "
        self.navigationItem.backBarButtonItem = backItem
        
        switch role {
        case 1:
            let first = self.storyboard?.instantiateViewController(withIdentifier: "WMain") as! WMainViewController
            self.navigationController?.pushViewController(first, animated: true)
        case 2:
            let driver = self.storyboard?.instantiateViewController(withIdentifier: "DMain") as! DMainViewController
            self.navigationController?.pushViewController(driver, animated: true)
        case 3:
            let market = self.storyboard?.instantiateViewController(withIdentifier: "MInventory") as! MInventoryViewController
            self.navigationController?.pushViewController(market, animated: true)
        default:
            showMessage("The user role is incorrect, please try again.")
        }
        
    }
    
    //MARK: Core Data for User
    func saveUserLogedIn(_ role: Int, userId: Int, userName: String) {
        do {
            try Locksmith.updateData(data: ["role": role, "id": userId, "name": userName], forUserAccount: "user")
        } catch {
            
        }
    }
    
    func validateUserLoggedIn() {
        if let dictionary = Locksmith.loadDataForUserAccount(userAccount: "user") {
            let role = dictionary["role"] as! Int
            userName     = dictionary["name"] as! String
            userGlobal   = "\(dictionary["id"] as! Int)"
            self.processUserScreen(role)
        }
    }
    
    func checkIfNotification() {
        if let dictionary = Locksmith.loadDataForUserAccount(userAccount: "notification") {
            let role = dictionary["role"] as! Int
            if role == 3 {
                let out = self.storyboard?.instantiateViewController(withIdentifier: "OrderRequest") as! OrderRequestViewController
                self.navigationController?.pushViewController(out, animated: true)
            }
        } else {
            self.validateUserLoggedIn()
        }
    }

}

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.hideKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    func hideKeyboard() {
        view.endEditing(true)
    }
}

//
//  OrderRequestViewController.swift
//  PerfectPick
//
//  Created by Jhon Villalobos on 7/6/16.
//  Copyright © 2016 Jhon Villalobos. All rights reserved.
//

import UIKit
import SwiftSpinner
import Locksmith

class OrderRequestViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UITabBarDelegate {

    @IBOutlet weak var titlesContainer: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var buttonSendOrder: UIButton!
    @IBOutlet weak var buttonSelect: UIButton!
    @IBOutlet weak var tabBar: UITabBar!
    
    var refreshControl : UIRefreshControl!
    
    var orderList = [Order]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        checkIfWarehouse()

        self.title = "Order Request"
        self.tabBar.delegate = self
        self.tabBar.barTintColor = UIColor.black
        
        self.titlesContainer.tintColor = UIColor.white
        self.titlesContainer.backgroundColor = UIColor(colorLiteralRed: 60/255.0, green: 60/255.0, blue: 60/255.0, alpha: 1.0)
        self.titlesContainer.layer.cornerRadius = 5.0
        
        self.buttonSendOrder.layer.cornerRadius = 5.0
        self.buttonSelect.layer.cornerRadius = 5.0
        
        loadRefreshControl()
        
        self.getOrder()
    }
    
    //Receive the switch action from TableView
    func changeItemStatus(_ sender:UISwitch) {
        SwiftSpinner.show("updating...")
        let idRecord = sender.tag
        let statusItem = sender.isOn
        let parameters = ["key": API_KEY, "id": idRecord, "user": userGlobal, "status": statusItem] as AnyObject
        let methodCall = "\(URLServer)changeItemOrderedStatus"
        makeRequest(methodCall, metodo: .post, params: parameters) {
            response, error in
            if let ans = response {
                if ans["success"] as! Bool == true {
                    self.getOrder()
                } else {
                    self.showMessage(ans["message"] as! String)
                }
            } else {
                self.showMessage(NETWORK_ERROR)
            }
        }
        SwiftSpinner.hide()
    }
    
    // MARK: TableView Delegates
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WOrderCell") as! OrderRequestTableViewCell
        
        let itemRow = self.orderList[indexPath.row]
        cell.itemName.text  = itemRow.itemName
        cell.itemBoxes.text = "\(itemRow.qty!)"
        cell.itemState.isOn = itemRow.status
        cell.itemState.tag = itemRow.id
        cell.itemState.addTarget(self, action: #selector(OrderRequestViewController.changeItemStatus), for: .valueChanged)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        var numSections = 0
        if (self.orderList.count > 0) {
            numSections = 1
        } else {
            let noDataLabel: UILabel = UILabel.init(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
            noDataLabel.text = "There no order available yet. \n Reasons Why? \n 1. Market never made an order. \n 2. Market has yet to send order request to you."
            noDataLabel.numberOfLines = 6
            noDataLabel.textColor = UIColor.red
            noDataLabel.textAlignment = .center
            tableView.backgroundView = noDataLabel
            tableView.separatorStyle = .none
        }
        return numSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.orderList.count
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: network requests
    func getOrder() {
        let parameters = ["key": API_KEY, "status": 1] as AnyObject
        let methodCall = "\(URLServer)getOrder"
        makeRequest(methodCall, metodo: .get, params: parameters) {
            response, error in
            if let ans = response {
                if ans["success"] as! Bool == true {
                    let content = ans["content"] as! [[String:AnyObject]]
                    self.orderList.removeAll()
                    if content.count > 0 {
                        self.buttonSelect.isHidden = false
                        for prd in content {
                            let idRow    = Int(prd["id"] as! String)!
                            let qtyItem  = Int(prd["quantity"] as! String)!
                            let qtyConfirmed = Int(prd["quantity"] as! String)!
                            let dateItem = convertStringDate(prd["date"] as! String)
                            let locat    = Int(prd["locations_id"] as! String)!
                            let itId     = Int(prd["items_id"] as! String)!
                            let userId   = Int(prd["users_id"] as! String)!
                            let itemName = prd["name"] as! String
                            let orderId  = Int(prd["orders_id"] as! String)!
                            let itemStatus = Int(prd["confirmed"] as! String)
                            let locationName = prd["location"] as! String
                            let switchStatus = (itemStatus == 1 ? true : false)
                            
                            let fillingList : Order = Order(id: idRow, qty: qtyItem, qtyConfirmed: qtyConfirmed, date: dateItem, locId: locat, itemId: itId, marketId: userId, itemName: itemName, status: switchStatus, orderId: orderId, locationName: locationName)
                            self.orderList.append(fillingList)
                        }
                    } else {
                        self.buttonSelect.isHidden = true
                        //self.showMessage("No pending orders")
                    }
                    self.do_cell_refresh()
                } else {
                    self.showMessage(ans["message"] as! String)
                }
            } else {
                self.showMessage(NETWORK_ERROR)
            }
        }
    }
    
    func do_cell_refresh() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView?.reloadData()
        return
    }
    
    func showMessage(_ message: String) {
        let alert = UIAlertController(title: APP_NAME, message: message, preferredStyle: .alert)
        let cancel = UIAlertAction(title: "Close", style: .cancel, handler: { (action) -> Void in })
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func buttonSelectAction(_ sender: AnyObject) {
        SwiftSpinner.show("updating...")
        if self.orderList.count > 0 {
            for i in 0 ..< self.orderList.count {
                let idRecord = self.orderList[i].id
                let itemStatus = (self.orderList[i].status == true ? false : true)
                let parameters = ["key": API_KEY, "id": idRecord!, "user": userGlobal, "status": itemStatus] as AnyObject
                let methodCall = "\(URLServer)changeItemOrderedStatus"
                makeRequest(methodCall, metodo: .post, params: parameters) {
                    response, error in
                    if let ans = response {
                        if ans["success"] as! Bool == true {
                            self.getOrder()
                        } else {
                            //self.showMessage(ans["message"] as! String)
                        }
                    } else {
                        self.showMessage(NETWORK_ERROR)
                    }
                }
            }
        }
        SwiftSpinner.hide()
    }
    
    @IBAction func buttonSendOrder(_ sender: AnyObject) {
        SwiftSpinner.show("sending order...")
        processOrderPrompt()
        SwiftSpinner.hide()
    }
    
    
    func orderSentMessage(_ message: String) {
        let alert = UIAlertController(title: APP_NAME, message: message, preferredStyle: .alert)
        let close = UIAlertAction(title: "OK", style: .default, handler: {
            (action) -> Void in
            SwiftSpinner.show("loading...")
            self.getOrder()
            SwiftSpinner.hide()
        })
        alert.addAction(close)
        self.present(alert, animated: true, completion: nil)
    }
    
    func processOrderPrompt() {
        let alert = UIAlertController(title: APP_NAME, message: "Are you sure you loaded the truck?", preferredStyle: .alert)
        let cancel = UIAlertAction(title: "No", style: .cancel, handler: { (action) -> Void in })
        let doIt = UIAlertAction(title: "Yes", style: .default, handler: {
            (action) -> Void in
            
            if self.validateActiveItems() == true {
                let statusLibrary = Status.StatusLibrary().library
                let statusCode = statusLibrary["Loading"]!
                let marketId = self.orderList[0].marketId!
                let orderNumber = self.orderList[0].orderId!
  
                //Change status order
                let parameters = ["key": API_KEY, "marketid": marketId, "user": userGlobal, "status": statusCode, "order": orderNumber  ] as AnyObject
                let methodCall = "\(URLServer)changeOrderStatus"
                makeRequest(methodCall, metodo: .post, params: parameters) {
                    response, error in
                    if let ans = response {
                        if ans["success"] as! Bool == true {
                            self.orderSentMessage(ans["message"] as! String)
                        } else {
                            self.showMessage(ans["message"] as! String)
                        }
                    } else {
                        self.showMessage(NETWORK_ERROR)
                    }
                }
                
            } else {
                self.showMessage("You must select at least 1 item from the order list")
            }
            
        })
        alert.addAction(cancel)
        alert.addAction(doIt)
        self.present(alert, animated: true, completion: nil)
    }
    
    
    //Check if ar least 1 items is selected from the order list
    func validateActiveItems() -> Bool {
        var returnType = false
        for i in 0 ..< self.orderList.count {
            if self.orderList[i].status == true {
                returnType = true
                break
            }
        }
        return returnType
    }
    
    func loadRefreshControl() {
        refreshControl = UIRefreshControl()
        refreshControl.backgroundColor = UIColor.red
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to update data")
        refreshControl.addTarget(self, action: #selector(OrderRequestViewController.refreshTable), for: .valueChanged)
        self.tableView.addSubview(refreshControl)
    }
    
    func refreshTable() {
        getOrder()
        refreshControl.endRefreshing()
    }
    
    //MARK: Bottom Tabs actions
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        switch item.tag {
        case 1:
            let warehouse = self.storyboard?.instantiateViewController(withIdentifier: "WMain") as! WMainViewController
            self.navigationController?.pushViewController(warehouse, animated: true)
        case 2:
            let driver = self.storyboard?.instantiateViewController(withIdentifier: "WDriver") as! WDriverViewController
            driver.originRequest = 1
            self.navigationController?.pushViewController(driver, animated: true)
        case 3:
            print()
        default:
            return
        }
    }
    
    func checkIfWarehouse() {
        var role = 0
        if let dictionary = Locksmith.loadDataForUserAccount(userAccount: "user") {
            role = dictionary["role"] as! Int
        }
        
        if role != 1 {
            deleteLogOut()
            let out = self.storyboard?.instantiateViewController(withIdentifier: "Home") as! HomeViewController
            self.navigationController?.pushViewController(out, animated: true)
        }
    }

}

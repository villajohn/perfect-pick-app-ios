//
//  ProductOrder.swift
//  PerfectPick
//
//  Created by Jhon Villalobos on 2/1/17.
//  Copyright © 2017 Jhon Villalobos. All rights reserved.
//

import Foundation

//
//  Product.swift
//  InventoryApp
//
//  Created by Jhon Villalobos on 7/1/16.
//  Copyright © 2016 Jhon Villalobos. All rights reserved.
//

import Foundation

class ProductOrder {
    var selected: Bool!
    var id:Int!
    var name:String!
    var price:Double!
    var quantity:Double!
    var skids: Int!
    var totalBoxes: Int!
    var requested: Int!
    
    init(selected: Bool, id: Int, name: String, price: Double, quantity: Double, skids: Int, totalBoxes: Int, requested: Int) {
        self.selected = selected
        self.name     = name
        self.price    = price
        self.quantity = quantity
        self.id       = id
        self.requested = requested
    }
    
}

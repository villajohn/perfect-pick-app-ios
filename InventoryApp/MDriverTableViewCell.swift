//
//  MDriverTableViewCell.swift
//  PerfectPick
//
//  Created by Jhon Villalobos on 7/9/16.
//  Copyright © 2016 Jhon Villalobos. All rights reserved.
//

import UIKit

class MDriverTableViewCell: UITableViewCell {

    @IBOutlet weak var itemBoxes: UILabel!
    @IBOutlet weak var itemName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

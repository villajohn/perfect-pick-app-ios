//
//  Transit.swift
//  PerfectPick
//
//  Created by Jhon Villalobos on 7/7/16.
//  Copyright © 2016 Jhon Villalobos. All rights reserved.
//

import Foundation

class Transit {
    var id: Int!
    var requested: Int!
    var quantity: Int!
    var destination: Int!
    var pickUp: Int!
    var confirmed: Int!
    var itemId: Int!
    var truckId: Int!
    var marketId: Int!
    var itemName: String!
    
    init(id: Int, request: Int, qty: Int, dest: Int, pick: Int, confirm: Int, item: Int, truck: Int, market: Int, name: String) {
        self.id             = id
        self.requested      = request
        self.quantity       = qty
        self.destination    = dest
        self.pickUp         = pick
        self.confirmed      = confirm
        self.itemId         = item
        self.truckId        = truck
        self.marketId       = market
        self.itemName       = name
    }
}


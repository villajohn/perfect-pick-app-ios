//
//  Constants.swift
//  InventoryApp
//
//  Created by Jhon Villalobos on 6/30/16.
//  Copyright © 2016 Jhon Villalobos. All rights reserved.
//

import Foundation
import Alamofire
import Locksmith

//com.jack.perfectpick - 1.2.0 - build 5
//var URLServer       = "http://mipsicomama.com/perfectpick/Api/"
var URLServer       = "http://159.203.3.120/perfectpick/Api/"
//var URLServer       = "http://192.168.0.101/inventory/Api/"
var userName        = ""
var userGlobal      = ""
let APP_NAME        = "PerfectPick"
let API_KEY         = "Yn89-ttP6-m5Eu"
let NETWORK_ERROR   = "Your request can't be finished, try again"



func makeRequest(_ server: String, metodo: HTTPMethod, params: AnyObject, completionHandler: @escaping (NSDictionary?, NSError?) -> ()) {
    
    Alamofire.request(server, method: metodo, parameters: params as? [String: AnyObject])
        .responseJSON {
            response in
            
            switch response.result {
            case .success(let value):
                completionHandler(value as? NSDictionary, nil)
            case .failure(let error):
                completionHandler(nil, error as NSError?)
            }
    }
}

func makeRequest_(server: String, method: HTTPMethod, params: AnyObject, completionHandler: (NSDictionary?, NSError?) -> ()) {
    
    Alamofire.request(server, method: method, parameters: params as? [String: AnyObject])
        
        .responseString {
            response in
            switch response.result {
            case .success(let value):
                print(value)
            case .failure(let error):
                print(error)
            }
    }
}



func convertStringDate(_ string: String) -> Date {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy.MM.dd" /* find out and place date format from http://userguide.icu-project.org/formatparse/datetime */
    let date = dateFormatter.date(from: string)
    return date!
}

func deleteLogOut() {
    do {
        try Locksmith.deleteDataForUserAccount(userAccount: "user")
    } catch {}
}

func cleanNotification() {
    do {
        try Locksmith.deleteDataForUserAccount(userAccount: "notification")
    } catch {}
}

func globalMessage(_ msgtitle: NSString, msgBody: NSString, delegate: AnyObject?, self: UIViewController) {
    let alert: UIAlertController = UIAlertController()
    alert.title = msgtitle as String
    alert.message = msgBody as String
    let close   = UIAlertAction(title: "Ok", style: .cancel, handler: { (action) -> Void in })
    alert.addAction(close)
    self.present(alert, animated: true, completion: nil)
}
